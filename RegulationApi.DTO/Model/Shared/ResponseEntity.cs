﻿using System;

namespace RegulationApi.DTO.Model.Shared
{
    public class ResponseEntity
    {
        public Guid Id { get; set; }
    }
}