﻿using System.Linq;

namespace RegulationApi.DTO.Model
{
    public class BaseSearchOptions
    {
        public string[] Props { get; set; }

        public object[] GetProps()
        {
            return GetType().GetProperties()
                .Where(p => p.Name != nameof(Props))
                .Select(p => p.GetValue(this, null)).Concat(Props).ToArray();
        }
    }
}