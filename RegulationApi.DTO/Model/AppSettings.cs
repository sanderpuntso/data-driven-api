﻿namespace RegulationApi.DTO.Model
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}