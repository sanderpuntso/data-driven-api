﻿using System.Collections.Generic;
using RegulationApi.DAL.Model;

namespace RegulationApi.DTO.Model.Return
{
    public class ReturnCreationModel
    {
        public List<ReturnProduct> ReturnProducts { get; set; }
    }
}