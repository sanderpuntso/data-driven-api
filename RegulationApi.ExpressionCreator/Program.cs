﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Aq.ExpressionJsonSerializer;
using Newtonsoft.Json;

namespace RegulationApi.ExpressionCreator
{
    public class Program
    {
        static void Main(string[] args)
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            settings.Converters.Add(new ExpressionJsonConverter(
                Assembly.GetAssembly(typeof(Program))
            ));

            var expression = Expression.IfThenElse(
                Expression.Constant(true),
                Expression.Constant("Tõene"),
                Expression.Constant("Väär")
            );

            var serializeObject = JsonConvert.SerializeObject(expression, settings);

            Console.Write(serializeObject);
        }
    }
}
