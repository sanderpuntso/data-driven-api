﻿using AutoMapper;
using DataDrivenApi.DTO.Model.Assignment;
using DataDrivenApi.DTO.Model.Grade;
using DataDrivenApi.DTO.Model.Group;
using DataDrivenApi.DTO.Model.Institution;
using DataDrivenApi.DTO.Model.Member;
using DataDrivenApi.DTO.Model.Session;
using DataDrivenApi.DTO.Model.Tenant;
using DataDrivenApi.DTO.Model.User;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Mappers
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<DAL.Model.Tenant, TenantResponse>();
            CreateMap<User, UserResponse>();
            CreateMap<Institution, InstitutionResponse>();
            CreateMap<InstitutionMember, InstitutionMemberResponse>();
            CreateMap<Group, GroupResponse>();
            CreateMap<GroupMember, GroupMemberResponse>();
            CreateMap<Session, SessionResponse>();
            CreateMap<SessionMember, SessionMemberResponse>();
            CreateMap<Assignment, AssignmentResponse>();
            CreateMap<Grade, GradeResponse>();
        }
    }
}