﻿using System;
using System.Linq;
using System.Security.Claims;
using DataDrivenApi.DTO.Model.Assignment;
using DataDrivenApi.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataDrivenApi.Controllers
{
    [ApiController]
    [Authorize]
    [Route("assignment")]
    public class AssignmentController : ControllerBase
    {
        private readonly IAssignmentService _assignmentService;

        public AssignmentController(IAssignmentService assignmentService)
        {
            _assignmentService = assignmentService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] AssignmentCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_assignmentService.Add(tenantId, actorId, model));
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_assignmentService.Get(tenantId, actorId, id));
        }

        [HttpGet]
        public IActionResult Get([FromQuery] AssignmentSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_assignmentService.GetAssignments(tenantId, actorId, options));
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] Guid id, [FromBody] AssignmentCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_assignmentService.Update(tenantId, actorId, id, model));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_assignmentService.Delete(tenantId, actorId, id));
        }
    }
}