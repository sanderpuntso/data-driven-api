﻿using System;
using System.Linq;
using System.Security.Claims;
using DataDrivenApi.DTO.Model.Group;
using DataDrivenApi.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataDrivenApi.Controllers
{
    [ApiController]
    [Authorize]
    [Route("group")]
    public class GroupController : ControllerBase
    {
        private readonly IGroupService _groupService;

        public GroupController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult Post([FromBody] GroupCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.Add(tenantId, actorId, model));
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.Get(tenantId, actorId, id));
        }

        [HttpGet]
        public IActionResult Get([FromQuery] GroupSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.GetGroups(tenantId, actorId, options));
        }

        [HttpPut]
        [Route("{id}")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult Put([FromRoute] Guid id, [FromBody] GroupCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.Update(tenantId, actorId, id, model));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.Delete(tenantId, actorId, id));
        }

        [HttpPost("member")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult PostMember([FromBody] GroupMemberCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.AddGroupMember(tenantId, actorId, model));
        }

        [HttpGet("member/{id}")]
        public IActionResult GetMember([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.GetGroupMember(tenantId, actorId, id));
        }

        [HttpGet("member")]
        public IActionResult GetMembers([FromQuery] GroupMemberSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.GetGroupMembers(tenantId, actorId, options));
        }

        [HttpPut("member/{id}")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult PutMember([FromRoute] Guid id, [FromBody] GroupMemberCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.UpdateGroupMember(tenantId, actorId, id, model));
        }

        [HttpDelete("member/{id}")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult DeleteMember([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_groupService.DeleteGroupMember(tenantId, actorId, id));
        }
    }
}