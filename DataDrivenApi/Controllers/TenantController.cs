﻿using System;
using System.Linq;
using System.Security.Claims;
using DataDrivenApi.DTO.Model.Tenant;
using DataDrivenApi.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataDrivenApi.Controllers
{
    [ApiController]
    [Route("tenant")]
    [Authorize(Roles = "Admin")]
    public class TenantController : ControllerBase
    {
        private readonly ITenantService _tenantService;

        public TenantController(ITenantService tenantService)
        {
            _tenantService = tenantService;
        }

        [HttpPost("authenticate")]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] TenantAuthenticationModel model)
        {
            var token = _tenantService.Authenticate(model);
            return Ok(token);
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] TenantCreationModel model)
        {
            return Ok(_tenantService.Add(model));
        }

        [HttpGet]
        public IActionResult Get()
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            return Ok(_tenantService.Get(tenantId));
        }
        
        [HttpPut]
        public IActionResult Put([FromBody] TenantUpdateModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_tenantService.Update(tenantId, actorId, model));
        }

        [HttpDelete]
        public IActionResult Delete()
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_tenantService.Delete(tenantId, actorId));
        }
    }
}