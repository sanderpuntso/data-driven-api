﻿using System;
using System.Linq;
using System.Security.Claims;
using DataDrivenApi.DTO.Model.Session;
using DataDrivenApi.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataDrivenApi.Controllers
{
    [ApiController]
    [Authorize]
    [Route("session")]
    public class SessionController : ControllerBase
    {
        private readonly ISessionService _sessionService;

        public SessionController(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] SessionCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.Add(tenantId, actorId, model));
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.Get(tenantId, actorId, id));
        }

        [HttpGet]
        public IActionResult Get([FromQuery] SessionSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.GetSessions(tenantId, actorId, options));
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] Guid id, [FromBody] SessionCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.Update(tenantId, actorId, id, model));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.Delete(tenantId, actorId, id));
        }

        [HttpPost("member")]
        public IActionResult AddMember([FromBody] SessionMemberCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.AddSessionMember(tenantId, actorId, model));
        }

        [HttpGet("member/{id}")]
        public IActionResult GetMember([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.GetSessionMember(tenantId, actorId, id));
        }

        [HttpGet("member")]
        public IActionResult GetMembers([FromQuery] SessionMemberSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.GetSessionMembers(tenantId, actorId, options));
        }

        [HttpPut("member/{id}")]
        public IActionResult PutMember([FromRoute] Guid id, [FromBody] SessionMemberCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.UpdateSessionMember(tenantId, actorId, id, model));
        }

        [HttpDelete("member/{id}")]
        public IActionResult DeleteMember([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_sessionService.DeleteSessionMember(tenantId, actorId, id));
        }
    }
}