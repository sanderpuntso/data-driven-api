﻿using System;
using System.Linq;
using System.Security.Claims;
using DataDrivenApi.DTO.Model.Institution;
using DataDrivenApi.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataDrivenApi.Controllers
{
    [ApiController]
    [Authorize(Roles = "Admin")]
    [Route("institution")]
    public class InstitutionController : ControllerBase
    {
        private readonly IInstitutionService _institutionService;

        public InstitutionController(IInstitutionService institutionService)
        {
            _institutionService = institutionService;
        }

        [HttpPost]
        public IActionResult Post([FromBody] InstitutionCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.Add(tenantId, actorId, model));
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.Get(tenantId, actorId, id));
        }

        [HttpGet]
        public IActionResult Get([FromQuery] InstitutionSearchOptions options, [FromQuery] string[] props)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.GetInstitutions(tenantId, actorId, options));
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromRoute] Guid id, [FromBody] InstitutionCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.Update(tenantId, actorId, id, model));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.Delete(tenantId, actorId, id));
        }
        
        [HttpPost("member")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult AddMember(InstitutionMemberCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.AddInstitutionMember(tenantId, actorId, model));
        }

        [HttpGet("member/{id}")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult GetMember([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.GetInstitutionMember(tenantId, actorId, id));
        }

        [HttpGet("member")]
        [Authorize(Roles = "Admin, Employee")]
        public IActionResult GetMember([FromQuery] InstitutionMemberSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.GetInstitutionMembers(tenantId, actorId, options));
        }

        [HttpPut("member/{id}")]
        public IActionResult PutMember([FromRoute] Guid id, [FromBody] InstitutionMemberCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.UpdateInstitutionMember(tenantId, actorId, id, model));
        }

        [HttpDelete("member/{id}")]
        public IActionResult DeleteMember([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_institutionService.DeleteInstitutionMember(tenantId, actorId, id));
        }
    }
}