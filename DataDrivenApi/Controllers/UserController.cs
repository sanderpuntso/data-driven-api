﻿using System;
using System.Linq;
using System.Security.Claims;
using DataDrivenApi.DTO.Model.User;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DataDrivenApi.Controllers
{
    [ApiController]
    [Authorize]
    [Route("user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] UserAuthenticationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            return Ok(_userService.Authenticate(tenantId, model));
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            return Ok(_userService.Add(tenantId, model));
        }

        [HttpGet("{id}")]
        public IActionResult GetUser([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_userService.Get(tenantId, actorId, id));
        }

        [HttpPut("{id}")]
        public IActionResult PutUser([FromRoute] Guid id, [FromBody] UserUpdateModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);

            if (actorId != id || User.IsInRole(UserRole.Admin.ToString()))
                return Unauthorized();

            return Ok(_userService.Update(tenantId, actorId, id, model));
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult GetUsers([FromQuery] UserSearchOptions options)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_userService.GetUsers(tenantId, actorId, options));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser([FromRoute] Guid id)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);

            if (actorId != id || User.IsInRole(UserRole.Admin.ToString()))
                return Unauthorized();

            return Ok(_userService.Delete(tenantId, actorId, id));
        }

        [HttpPut("role")]
        [Authorize(Roles = "Admin")]
        public IActionResult PutUserRole([FromBody] UserRoleCreationModel model)
        {
            var tenantId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            var actorId = new Guid(User.Claims.First(c => c.Type == ClaimTypes.Actor).Value);
            return Ok(_userService.UpdateUserRole(tenantId, actorId, model));
        }
    }
}