﻿using DataDrivenApi.DAL;
using DataDrivenApi.Installers.Interface;
using DataDrivenApi.Tenant.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SharedDbContext = DataDrivenApi.DAL.SharedDbContext;

namespace DataDrivenApi.Installers
{
    public class DbInstaller : IServiceInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SharedDbContext>
            (options =>
                options.UseSqlServer(configuration.GetConnectionString("SharedDbContext")));
            services.AddDbContext<TenantDbContext>
            (options =>
                options.UseSqlServer(configuration.GetConnectionString("TenantDbContext")));
        }
    }
}