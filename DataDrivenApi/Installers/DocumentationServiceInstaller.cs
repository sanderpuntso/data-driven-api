﻿using DataDrivenApi.Installers.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;

namespace DataDrivenApi.Installers
{
    public class DocumentationServiceInstaller : IServiceInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API doc",
                    Version = "v1"
                });

                //x.AddSecurityDefinition("Bearer", new ApiKeyScheme
                //{
                //    Description = "JWT Authorization header using the bearer scheme",
                //    Name = "Authorization",
                //    In = "header",
                //    Type = "apiKey"
                //});
                //x.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>()
                //{
                //    { "Bearer", new string[0] }
                //});
            });
        }
    }
}