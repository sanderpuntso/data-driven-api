﻿using System.Text.Json.Serialization;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.Installers.Interface;
using DataDrivenApi.Service;
using DataDrivenApi.Service.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;

namespace DataDrivenApi.Installers
{
    public class ServiceInstaller : IServiceInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc().AddJsonOptions(options => {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.JsonSerializerOptions.IgnoreNullValues = true;
            });

            var appSettingsSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            services.AddScoped<IConnectionService, ConnectionService>();

            services.AddScoped<ITenantService, TenantService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IInstitutionService, InstitutionService>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IGradeService, GradeService>();
            services.AddScoped<IAssignmentService, AssignmentService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ITestService, TestService>();
        }
    }
}