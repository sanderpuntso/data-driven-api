﻿using System;
using Microsoft.AspNetCore.Mvc;
using RegulationApi.DAL.Model;
using RegulationApi.Service.Interface;

namespace RegulationApi.Controllers
{
    [ApiController]
    [Route("regulation")]
    public class RegulationController : ControllerBase
    {
        private readonly IRegulationService _regulationService;

        public RegulationController(IRegulationService regulationService)
        {
            _regulationService = regulationService;
        }

        /// <summary>
        /// Create regulation. Regulation is invoked based on distribution and it's return.
        /// Each StoreProduct is iterated and if there were returned products for same Product, then
        /// the quantity of StoreProduct is regulated.
        /// </summary>
        /// <param name="distributionId"></param>
        /// <returns></returns>
        [HttpPost("distribution/{distributionId}")]
        public IActionResult CreateRegulation([FromRoute] Guid distributionId)
        {
            return Ok(_regulationService.Create(distributionId));
        }

        /// <summary>
        /// Create regulation with inline defined expression. Main functionality is the same, but the calculation
        /// of quantity of StoreProduct is calculated with evaluating Expression value in foreach loop.
        /// </summary>
        /// <param name="distributionId"></param>
        /// <returns>store</returns>
        [HttpPost("distribution/{distributionId}/inline")]
        public IActionResult CreateRegulationWithInlineExpression([FromRoute] Guid distributionId)
        {
            return Ok(_regulationService.CreateWithInlineExpression(distributionId));
        }

        /// <summary>
        /// Create regulation with fully data-driven input. Lambda Expression with DynamicInvoke is invoked
        /// in foreach loop with inputs StoreProduct and ReturnProduct. ReturnProduct values can be null, if
        /// product was not returned (sold 100%) or ReturnProduct entity.
        /// This API endpoint is fully data-driven meaning, that the quantity of StoreProduct or any
        /// other entity is not calculated - this should be done via input expression.
        /// </summary>
        /// <param name="distributionId"></param>
        /// <param name="expression"></param>
        /// <returns>store</returns>
        [HttpPost("distribution/{distributionId}/input")]
        public IActionResult CreateRegulationWithInputExpression([FromRoute] Guid distributionId, [FromBody] EntityExpression expression)
        {
            return Ok(_regulationService.CreateWithInputExpression(distributionId, expression));
        }
    }
}