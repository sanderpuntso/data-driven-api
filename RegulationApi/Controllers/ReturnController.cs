﻿using System;
using Microsoft.AspNetCore.Mvc;
using RegulationApi.DTO.Model.Return;
using RegulationApi.Service.Interface;

namespace RegulationApi.Controllers
{
    [ApiController]
    [Route("return")]
    public class ReturnController : ControllerBase
    {
        private readonly IReturnService _returnService;

        public ReturnController(IReturnService returnService)
        {
            _returnService = returnService;
        }

        [HttpGet("{id}")]
        public IActionResult GetReturn([FromRoute] Guid id)
        {
            return Ok(_returnService.Get(id));
        }

        [HttpPost("store/{storeId}/distribution/{distributionId}")]
        public IActionResult CreateStoreReturn([FromRoute] Guid storeId, [FromRoute] Guid distributionId, [FromBody] ReturnCreationModel model)
        {
            return Ok(_returnService.Post(storeId, distributionId, model.ReturnProducts));
        }

        [HttpPut("{id}")]
        public IActionResult UpdateStoreDistribution([FromRoute] Guid id, [FromBody] ReturnCreationModel model)
        {
            return Ok(_returnService.Update(id, model.ReturnProducts));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteDistribution([FromBody] Guid id)
        {
            return Ok(_returnService.Delete(id));
        }
    }
}