﻿using System;
using Microsoft.AspNetCore.Mvc;
using RegulationApi.DAL.Model;
using RegulationApi.Service.Interface;

namespace RegulationApi.Controllers
{
    [ApiController]
    [Route("distribution")]
    public class DistributionController : ControllerBase
    {
        private readonly IDistributionService _distributionService;

        public DistributionController(IDistributionService distributionService)
        {
            _distributionService = distributionService;
        }

        [HttpGet("{id}")]
        public IActionResult GetDistribution([FromRoute] Guid id)
        {
            return Ok(_distributionService.Get(id));
        }

        [HttpPost("{id}")]
        public IActionResult DataDrivenGetDistribution([FromRoute] Guid id, [FromBody] EntityExpression expression)
        {
            return Ok(_distributionService.Get(id, expression));
        }

        [HttpPost("store/{storeId}")]
        public IActionResult CreateStoreDistribution([FromRoute] Guid storeId)
        {
            return Ok(_distributionService.Post(storeId));
        }

        [HttpPut("{id}")]
        public IActionResult UpdateStoreDistribution([FromRoute] Guid id)
        {
            return Ok(_distributionService.Update(id));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteDistribution([FromBody] Guid id)
        {
            return Ok(_distributionService.Delete(id));
        }
    }
}