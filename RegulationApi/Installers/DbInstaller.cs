﻿using RegulationApi.Installers.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RegulationDbContext = RegulationApi.DAL.RegulationDbContext;

namespace RegulationApi.Installers
{
    public class DbInstaller : IServiceInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<RegulationDbContext>
            (options =>
                options.UseSqlServer(configuration.GetConnectionString("RegulationDbContext")));
        }
    }
}