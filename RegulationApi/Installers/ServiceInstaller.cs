﻿using System.Reflection;
using System.Text.Json.Serialization;
using Aq.ExpressionJsonSerializer;
using RegulationApi.DTO.Model;
using RegulationApi.Installers.Interface;
using RegulationApi.Service;
using RegulationApi.Service.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace RegulationApi.Installers
{
    public class ServiceInstaller : IServiceInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddMvc()
                .AddNewtonsoftJson(x =>
                {
                    x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    x.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                    x.SerializerSettings.Converters.Add(new ExpressionJsonConverter(
                        Assembly.GetAssembly(typeof(RegulationService))
                    ));
                });

            var appSettingsSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            services.AddScoped<IRegulationService, RegulationService>();
            services.AddScoped<IDistributionService, DistributionService>();
            services.AddScoped<IReturnService, ReturnService>();
        }
    }
}