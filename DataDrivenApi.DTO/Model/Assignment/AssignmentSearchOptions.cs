﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Assignment
{
    public class AssignmentSearchOptions
    {
        public string Description { get; set; }
        public DateTime? DueDate { get; set; }
        public Guid? SessionId { get; set; }
        public Guid? UserId { get; set; }
        public MemberType? MemberType { get; set; }
    }
}