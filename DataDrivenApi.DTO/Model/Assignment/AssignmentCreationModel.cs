﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DataDrivenApi.DTO.Model.Assignment
{
    public class AssignmentCreationModel
    {
        [Required]
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [Required]
        [JsonPropertyName("dueDate")]
        public DateTime DueDate { get; set; }
        [Required]
        [JsonPropertyName("sessionId")]
        public Guid SessionId { get; set; }
    }
}