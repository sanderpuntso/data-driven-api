﻿using System;
using DataDrivenApi.DTO.Model.Shared;

namespace DataDrivenApi.DTO.Model.Assignment
{
    public class AssignmentResponse : ResponseEntity
    {
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public Guid SessionId { get; set; }
    }
}