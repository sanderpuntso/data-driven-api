﻿using System;
using System.Text.Json.Serialization;
using DataDrivenApi.DTO.Model.Shared;
using DataDrivenApi.Tenant.DAL.Enums;

namespace DataDrivenApi.DTO.Model.User
{
    public class UserResponse : ResponseEntity
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public UserRole Role { get; set; }
    }
}