﻿using System.Text.Json.Serialization;
using DataDrivenApi.Tenant.DAL.Enums;

namespace DataDrivenApi.DTO.Model.User
{
    public class UserSearchOptions
    {
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }
        [JsonPropertyName("role")]
        public UserRole Role { get; set; }
        [JsonPropertyName("email")]
        public string Email { get; set; }
    }
}