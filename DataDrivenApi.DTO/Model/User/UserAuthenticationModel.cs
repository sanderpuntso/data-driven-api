﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DataDrivenApi.DTO.Model.User
{
    public class UserAuthenticationModel
    {
        [Required]
        [JsonPropertyName("email")]
        public string Email { get; set; }

        [Required]
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}