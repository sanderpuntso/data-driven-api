﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DataDrivenApi.Tenant.DAL.Enums;

namespace DataDrivenApi.DTO.Model.User
{
    public class UserRoleCreationModel
    {
        [DisplayName("userId")]
        [Required]
        public Guid? UserId { get; set; }
        [DisplayName("role")]
        [Required]
        public UserRole Role { get; set; }
    }
}