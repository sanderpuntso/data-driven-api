﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DataDrivenApi.DTO.Model.Tenant
{
    public class TenantCreationModel
    {
        [Required, StringLength(100, MinimumLength = 4)]
        public string Email { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, StringLength(100, MinimumLength = 8)]
        public string Name { get; set; }

        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        
        [Required, StringLength(100, MinimumLength = 8)]
        public string HostName { get; set; }
    }
}