﻿using System.Text.Json.Serialization;
using DataDrivenApi.DTO.Model.Shared;

namespace DataDrivenApi.DTO.Model.Tenant
{
    public class TenantResponse : ResponseEntity
    {
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("hostname")]
        public string HostName { get; set; }
    }
}