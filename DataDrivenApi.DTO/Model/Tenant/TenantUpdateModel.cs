﻿using System.ComponentModel.DataAnnotations;

namespace DataDrivenApi.DTO.Model.Tenant
{
    public class TenantUpdateModel
    {
        [Required, StringLength(100, MinimumLength = 4)]
        public string Email { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, StringLength(100, MinimumLength = 8)]
        public string Name { get; set; }
                
        [Required, StringLength(100, MinimumLength = 8)]
        public string HostName { get; set; }
    }
}