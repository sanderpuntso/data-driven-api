﻿using System.Text.Json.Serialization;

namespace DataDrivenApi.DTO.Model.Tenant
{
    public class TenantRegistrationResponse
    {
        [JsonPropertyName("token")]
        public string Token { get; set; }
        [JsonPropertyName("username")] 
        public string UserName { get; set; }
    }
}