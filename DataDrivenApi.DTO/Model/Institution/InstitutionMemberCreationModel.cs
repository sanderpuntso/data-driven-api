﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Institution
{
    public class InstitutionMemberCreationModel
    {
        public Guid InstitutionId { get; set; }
        public Guid UserId { get; set; }
        public MemberType MemberType { get; set; }
    }
}