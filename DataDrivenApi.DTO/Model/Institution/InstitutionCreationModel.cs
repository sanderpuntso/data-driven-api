﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace DataDrivenApi.DTO.Model.Institution
{
    public class InstitutionCreationModel
    {
        [Required]
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("type")]
        public string Type { get; set; }
    }
}