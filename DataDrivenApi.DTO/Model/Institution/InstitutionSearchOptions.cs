﻿using System;
using System.Text.Json.Serialization;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Institution
{
    public class InstitutionSearchOptions : BaseSearchOptions
    {
        public Guid? InstitutionId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        /// <summary>
        /// For searching users' sessions
        /// </summary>
        public Guid? UserId { get; set; }
        /// <summary>
        /// For searching users' sessions with specific role
        /// </summary>
        public MemberType? MemberType { get; set; }
    }
}