﻿using System;
using System.Text.Json.Serialization;
using DataDrivenApi.DTO.Model.Shared;

namespace DataDrivenApi.DTO.Model.Institution
{
    public class InstitutionResponse : ResponseEntity
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}