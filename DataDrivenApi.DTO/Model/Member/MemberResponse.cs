﻿using System;
using System.Text.Json.Serialization;
using DataDrivenApi.DTO.Model.Shared;
using DataDrivenApi.Tenant.DAL.Enums;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Member
{
    public class MemberResponse : ResponseEntity
    {
        [JsonPropertyName("userId")]
        public Guid UserId { get; set; }
        [JsonPropertyName("role")]
        public UserRole Role { get; set; }
    }
    public class GroupMemberResponse : MemberResponse
    {
        [JsonPropertyName("groupId")]
        public Guid GroupId { get; set; }
    }
    public class InstitutionMemberResponse : MemberResponse
    {
        [JsonPropertyName("institutionId")]
        public Guid InstitutionId { get; set; }
    }

    public class SessionMemberResponse : MemberResponse
    {
        [JsonPropertyName("sessionId")]
        public Guid SessionId { get; set; }
    }
}