﻿using System;
using System.Text.Json.Serialization;

namespace DataDrivenApi.DTO.Model.Shared
{
    public class ResponseEntity
    {
        public Guid Id { get; set; }
    }
}