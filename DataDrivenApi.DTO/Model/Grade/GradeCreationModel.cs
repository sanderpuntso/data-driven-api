﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataDrivenApi.DTO.Model.Grade
{
    public class GradeCreationModel
    {

        [Required]
        public string Mark { get; set; }
        public string Description { get; set; }
        [Required]
        public Guid AssignmentId { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public Guid GraderId { get; set; }
    }
}