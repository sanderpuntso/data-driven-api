﻿using System;
using DataDrivenApi.DTO.Model.Shared;

namespace DataDrivenApi.DTO.Model.Grade
{
    public class GradeResponse : ResponseEntity
    {
        public string Mark { get; set; }
        public DateTime Grading { get; set; }
        public string Description { get; set; }
        public Guid AssignmentId { get; set; }
        public Guid UserId { get; set; }
        public Guid GraderId { get; set; }
    }
}