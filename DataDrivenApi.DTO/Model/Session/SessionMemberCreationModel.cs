﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Session
{
    public class SessionMemberCreationModel
    {
        public Guid SessionId { get; set; }
        public Guid UserId { get; set; }
        public MemberType MemberType { get; set; }
    }
}