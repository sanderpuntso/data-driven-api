﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataDrivenApi.DTO.Model.Session
{
    public class SessionCreationModel
    {
        [Required]
        public string Topic { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public string Place { get; set; }
        [Required]
        public Guid GroupId { get; set; }
    }
}