﻿using System;
using DataDrivenApi.DTO.Model.Shared;

namespace DataDrivenApi.DTO.Model.Session
{
    public class SessionResponse : ResponseEntity
    {
        public DateTime Time { get; set; }
        public string Place { get; set; }
        public Guid GroupId { get; set; }
    }
}
