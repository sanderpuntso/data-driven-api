﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Session
{
    public class SessionMemberSearchOptions
    {
        public Guid? SessionMemberId { get; set; }
        public Guid? UserId { get; set; }
        public MemberType? MemberType { get; set; }
    }
}