﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Session
{
    public class SessionSearchOptions
    {
        public Guid? SessionId { get; set; }
        public string Topic { get; set; }
        public string Subject { get; set; }
        public DateTime? Time { get; set; }
        public string Place { get; set; }
        public Guid? GroupId { get; set; }
        /// <summary>
        /// For searching users' sessions
        /// </summary>
        public Guid? UserId { get; set; }
        /// <summary>
        /// For searching users' sessions with specific role
        /// </summary>
        public MemberType? MemberType { get; set; }
    }
}