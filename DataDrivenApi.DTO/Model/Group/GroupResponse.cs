﻿using System;
using DataDrivenApi.DTO.Model.Shared;

namespace DataDrivenApi.DTO.Model.Group
{
    public class GroupResponse : ResponseEntity
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public Guid InstitutionId { get; set; }
    }
}
