﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Group
{
    public class GroupMemberSearchOptions
    {
        public Guid? GroupMemberId { get; set; }
        public Guid? UserId { get; set; }
        public MemberType? MemberType { get; set; }
    }
}