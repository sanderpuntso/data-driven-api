﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Group
{
    public class GroupSearchOptions
    {
        public Guid? GroupId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public Guid? ParentGroupId { get; set; }
        public Guid? InstitutionId { get; set; }
        /// <summary>
        /// For searching users' groups
        /// </summary>
        public Guid? UserId { get; set; }
        /// <summary>
        /// For searching users' groups with special role
        /// </summary>
        public MemberType? MemberType { get; set; }
    }
}