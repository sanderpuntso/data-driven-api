﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataDrivenApi.DTO.Model.Group
{
    public class GroupCreationModel
    {
        [Required]
        [StringLength(100, MinimumLength = 8)]
        public string Name { get; set; }
        public string Type { get; set; }
        [Required]
        public Guid InstitutionId { get; set; }
        public Guid? ParentId { get; set; }
    }
}