﻿using System;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.DTO.Model.Group
{
    public class GroupMemberCreationModel
    {
        public Guid GroupId { get; set; }
        public Guid UserId { get; set; }
        public MemberType MemberType { get; set; }
    }
}