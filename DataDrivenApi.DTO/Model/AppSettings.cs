﻿namespace DataDrivenApi.DTO.Model
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}