﻿using System.Collections.Generic;
using System.Linq;

namespace DataDrivenApi.DTO.Model
{
    public class MessagePackEntity<T>
    {
        public bool HasErrors => ErrorMessages.Any();

        public List<string> ErrorMessages { get; set; } = new List<string>();
        public List<string> InfoMessages { get; set; } = new List<string>();

        public T Result { get; set; }
    }
}