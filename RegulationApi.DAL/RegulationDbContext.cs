﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;
using RegulationApi.DAL.Model;

namespace RegulationApi.DAL
{
    public class RegulationDbContext : DbContext
    {
        private readonly string _connectionString;

        public RegulationDbContext(DbContextOptions<RegulationDbContext> options)
            : base(options)
        {
            _connectionString = options.GetExtension<SqlServerOptionsExtension>().ConnectionString;
        }

        public RegulationDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var product1 = new Product
            {
                Id = new Guid("1239c984-9579-43a9-8aeb-02c50cbe970f"),
                Name = "Eesti päevaleht",
                Price = 0.99m
            };
            var product2 = new Product
            {
                Id = new Guid("4e26d63c-18f7-4240-a53d-31f5a67a499a"),
                Name = "Kroonika",
                Price = 1.19m
            };

            var product3 = new Product
            {
                Id = new Guid("369e4b1e-1f96-4457-8aa1-308cf2f91144"),
                Name = "Tudengileht",
                Price = 0
            };

            modelBuilder.Entity<Product>().HasData(
                product1, product2, product3
            );

            var store1 = new Store
            {
                Id = new Guid("62eef5ae-0c22-4fc1-aa79-4124ab298eec"),
                Name = "R-Kiosk"
            };

            modelBuilder.Entity<Store>().HasData(store1);

            var storeProduct1 = new StoreProduct
            {
                Id = new Guid("5625b3cd-72cb-4dbc-b261-58160c225e0b"),
                StoreId = store1.Id,
                ProductId = new Guid("1239c984-9579-43a9-8aeb-02c50cbe970f"),
                Quantity = 200
            };
            var storeProduct2 = new StoreProduct
            {
                Id = new Guid("596799cc-eb37-4a96-bc46-6af6d13b011b"),
                StoreId = store1.Id,
                ProductId = new Guid("4e26d63c-18f7-4240-a53d-31f5a67a499a"),
                Quantity = 50
            };

            var storeProduct3 = new StoreProduct
            {
                Id = new Guid("51eaf777-0b37-4baf-b160-253f368a6a0f"),
                StoreId = store1.Id,
                ProductId = new Guid("369e4b1e-1f96-4457-8aa1-308cf2f91144"),
                Quantity = 400
            };

            modelBuilder.Entity<StoreProduct>().HasData(storeProduct1, storeProduct2, storeProduct3);

            modelBuilder.Entity<Store>()
                .HasMany(s => s.Returns)
                .WithOne(r => r.Store)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Return>()
                .HasOne(r => r.Store)
                .WithMany(s => s.Returns)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Store>()
                .HasMany(s => s.Distributions)
                .WithOne(d => d.Store)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Distribution>()
                .HasOne(d => d.Store)
                .WithMany(s => s.Distributions)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Return>()
                .HasOne(r => r.Distribution)
                .WithOne(d => d.Return)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Distribution>()
                .HasOne(d => d.Return)
                .WithOne(r => r.Distribution)
                .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<StoreProduct>(entity => { entity.HasIndex(storeProduct => new { storeProduct.ProductId, storeProduct.StoreId }).IsUnique(); });
            modelBuilder.Entity<ReturnProduct>(entity => { entity.HasIndex(returnProduct => new { returnProduct.ProductId, returnProduct.ReturnId }).IsUnique(); });
            modelBuilder.Entity<DistributionProduct>(entity => { entity.HasIndex(distributionProduct => new { distributionProduct.ProductId, distributionProduct.DistributionId }).IsUnique(); });
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Distribution> Distributions { get; set; }
        public DbSet<DistributionProduct> DistributionProducts { get; set; }
        public DbSet<Return> Returns { get; set; }
        public DbSet<ReturnProduct> ReturnProducts { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<StoreProduct> StoreProducts { get; set; }

        public DbSet<EntityExpression> Expressions { get; set; }
    }
}