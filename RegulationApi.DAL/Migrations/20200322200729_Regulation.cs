﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RegulationApi.DAL.Migrations
{
    public partial class Regulation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Stores",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Discount = table.Column<decimal>(type: "decimal(3,1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Distributions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Sum = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    StoreId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Distributions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Distributions_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StoreProducts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StoreProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StoreProducts_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DistributionProducts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    DistributionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributionProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistributionProducts_Distributions_DistributionId",
                        column: x => x.DistributionId,
                        principalTable: "Distributions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DistributionProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Returns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    DistributionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Returns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Returns_Distributions_DistributionId",
                        column: x => x.DistributionId,
                        principalTable: "Distributions",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Returns_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReturnProducts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    ReturnId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReturnProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReturnProducts_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReturnProducts_Returns_ReturnId",
                        column: x => x.ReturnId,
                        principalTable: "Returns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("1239c984-9579-43a9-8aeb-02c50cbe970f"), "Eesti päevaleht", 0.99m },
                    { new Guid("4e26d63c-18f7-4240-a53d-31f5a67a499a"), "Kroonika", 1.19m },
                    { new Guid("369e4b1e-1f96-4457-8aa1-308cf2f91144"), "Tudengileht", 0m }
                });

            migrationBuilder.InsertData(
                table: "Stores",
                columns: new[] { "Id", "Discount", "Name" },
                values: new object[] { new Guid("62eef5ae-0c22-4fc1-aa79-4124ab298eec"), null, "R-Kiosk" });

            migrationBuilder.InsertData(
                table: "StoreProducts",
                columns: new[] { "Id", "ProductId", "Quantity", "StoreId" },
                values: new object[] { new Guid("5625b3cd-72cb-4dbc-b261-58160c225e0b"), new Guid("1239c984-9579-43a9-8aeb-02c50cbe970f"), 200, new Guid("62eef5ae-0c22-4fc1-aa79-4124ab298eec") });

            migrationBuilder.InsertData(
                table: "StoreProducts",
                columns: new[] { "Id", "ProductId", "Quantity", "StoreId" },
                values: new object[] { new Guid("596799cc-eb37-4a96-bc46-6af6d13b011b"), new Guid("4e26d63c-18f7-4240-a53d-31f5a67a499a"), 50, new Guid("62eef5ae-0c22-4fc1-aa79-4124ab298eec") });

            migrationBuilder.InsertData(
                table: "StoreProducts",
                columns: new[] { "Id", "ProductId", "Quantity", "StoreId" },
                values: new object[] { new Guid("51eaf777-0b37-4baf-b160-253f368a6a0f"), new Guid("369e4b1e-1f96-4457-8aa1-308cf2f91144"), 400, new Guid("62eef5ae-0c22-4fc1-aa79-4124ab298eec") });

            migrationBuilder.CreateIndex(
                name: "IX_DistributionProducts_DistributionId",
                table: "DistributionProducts",
                column: "DistributionId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributionProducts_ProductId",
                table: "DistributionProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Distributions_StoreId",
                table: "Distributions",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnProducts_ProductId",
                table: "ReturnProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ReturnProducts_ReturnId",
                table: "ReturnProducts",
                column: "ReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_Returns_DistributionId",
                table: "Returns",
                column: "DistributionId");

            migrationBuilder.CreateIndex(
                name: "IX_Returns_StoreId",
                table: "Returns",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_StoreProducts_ProductId",
                table: "StoreProducts",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_StoreProducts_StoreId",
                table: "StoreProducts",
                column: "StoreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DistributionProducts");

            migrationBuilder.DropTable(
                name: "ReturnProducts");

            migrationBuilder.DropTable(
                name: "StoreProducts");

            migrationBuilder.DropTable(
                name: "Returns");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Distributions");

            migrationBuilder.DropTable(
                name: "Stores");
        }
    }
}
