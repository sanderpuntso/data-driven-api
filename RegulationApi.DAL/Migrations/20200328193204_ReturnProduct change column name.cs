﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RegulationApi.DAL.Migrations
{
    public partial class ReturnProductchangecolumnname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "ReturnProducts");

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "ReturnProducts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "ReturnProducts");

            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "ReturnProducts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
