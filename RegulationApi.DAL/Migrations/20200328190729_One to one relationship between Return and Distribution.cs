﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RegulationApi.DAL.Migrations
{
    public partial class OnetoonerelationshipbetweenReturnandDistribution : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Returns_DistributionId",
                table: "Returns");

            migrationBuilder.CreateIndex(
                name: "IX_Returns_DistributionId",
                table: "Returns",
                column: "DistributionId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Returns_DistributionId",
                table: "Returns");

            migrationBuilder.CreateIndex(
                name: "IX_Returns_DistributionId",
                table: "Returns",
                column: "DistributionId");
        }
    }
}
