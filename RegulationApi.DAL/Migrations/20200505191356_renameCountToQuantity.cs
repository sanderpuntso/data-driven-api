﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RegulationApi.DAL.Migrations
{
    public partial class renameCountToQuantity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discount",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "SerializedExpression",
                table: "Expressions");

            migrationBuilder.DropColumn(
                name: "Count",
                table: "DistributionProducts");

            migrationBuilder.AddColumn<int>(
                name: "Quantity",
                table: "DistributionProducts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "DistributionProducts");

            migrationBuilder.AddColumn<decimal>(
                name: "Discount",
                table: "Stores",
                type: "decimal(3,1)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SerializedExpression",
                table: "Expressions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "DistributionProducts",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
