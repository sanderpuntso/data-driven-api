﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RegulationApi.DAL.Migrations
{
    public partial class StoreProductUniqueIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StoreProducts_ProductId",
                table: "StoreProducts");

            migrationBuilder.CreateIndex(
                name: "IX_StoreProducts_ProductId_StoreId",
                table: "StoreProducts",
                columns: new[] { "ProductId", "StoreId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StoreProducts_ProductId_StoreId",
                table: "StoreProducts");

            migrationBuilder.CreateIndex(
                name: "IX_StoreProducts_ProductId",
                table: "StoreProducts",
                column: "ProductId");
        }
    }
}
