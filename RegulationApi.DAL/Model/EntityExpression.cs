﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace RegulationApi.DAL.Model
{
    public class EntityExpression
    {
        public Guid Id { get; set; }
        public string EntityName { get; set; }
        [NotMapped]
        public object SerializedExpression { get; set; }
    }
}
