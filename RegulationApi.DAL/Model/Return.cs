﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RegulationApi.DAL.Model
{
    public class Return
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public Guid StoreId { get; set; }
        public virtual Store Store { get; set; }
        public Guid DistributionId { get; set; }
        public virtual Distribution Distribution { get; set; }
        public virtual List<ReturnProduct> ReturnProducts { get; set; }
    }
}