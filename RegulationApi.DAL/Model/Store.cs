﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RegulationApi.DAL.Model
{
    public class Store
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        public string Name { get; set; }
        public virtual List<StoreProduct> StoreProducts { get; set; }
        public virtual List<Distribution> Distributions { get; set; }
        public virtual List<Return> Returns { get; set; }
    }
}
