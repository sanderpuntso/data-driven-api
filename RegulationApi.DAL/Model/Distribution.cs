﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RegulationApi.DAL.Model
{
    public class Distribution
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Sum { get; set; }
        public DateTime Created { get; set; } = DateTime.Now;
        public Guid StoreId { get; set; }
        public virtual Store Store { get; set; }
        public virtual List<DistributionProduct> DistributionProducts { get; set; }
        public virtual Return Return { get; set; }
    }
}