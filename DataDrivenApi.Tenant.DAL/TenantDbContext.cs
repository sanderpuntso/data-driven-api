﻿using System;
using DataDrivenApi.Tenant.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;

namespace DataDrivenApi.Tenant.DAL
{
    public class TenantDbContext : DbContext
    {
        private readonly string _connectionString;

        public TenantDbContext(DbContextOptions<TenantDbContext> options) : base(options)
        {
            _connectionString = options.GetExtension<SqlServerOptionsExtension>().ConnectionString;
        }

        public TenantDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>().HasOne(g => g.Parent)
                .WithMany(g => g.Groups)
                .HasForeignKey(g => g.ParentId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<Grade>()
                .HasOne(g => g.User)
                .WithMany(g => g.Grades)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Grade>()
                .HasOne(g => g.Grader)
                .WithMany(g => g.Graded)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<InstitutionMember>().HasKey(table => new {
                table.InstitutionId,
                table.UserId
            });
            modelBuilder.Entity<GroupMember>().HasKey(table => new {
                table.GroupId,
                table.UserId
            });
            modelBuilder.Entity<SessionMember>().HasKey(table => new {
                table.SessionId,
                table.UserId
            });
        }

        public bool EnsureCreated()
        {
            return Database.EnsureCreated();
        }

        public DbSet<Institution> Institutions { get; set; }
        public DbSet<InstitutionMember> InstitutionMembers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionMember> SessionMembers { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Grade> Grades { get; set; }

        public void Migrate()
        {
            Database.Migrate();
        }

        public bool TrySaveChanges(out string exception)
        {
            exception = null;
            try
            {
                SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                exception = e.Message;
                return false;
            }
        }
    }
}
