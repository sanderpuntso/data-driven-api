﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class Assignment : TenantEntity
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime DueDate { get; set; }
        [Required]
        public Guid SessionId { get; set; }
        public virtual Session Session { get; set; }
        public List<Grade> Grades { get; set; }
    }
}