﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class Session : TenantEntity
    {
        [Required]
        public string Topic { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public string Place { get; set; }
        [Required]
        public Guid GroupId { get; set; }
        public virtual Group Group { get; set; }
        public virtual List<Assignment> Assignments { get; set; }
        public virtual List<SessionMember> SessionMembers { get; set; }
    }
}