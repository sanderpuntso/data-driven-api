﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class Institution : TenantEntity
    {
        [Required]
        public string Name { get; set; }
        public string Type { get; set; }
        public virtual List<Group> Groups { get; set; }
        public virtual List<InstitutionMember> InstitutionMembers { get; set; }
        [NotMapped]
        public object Test { get; set; }
    }
}