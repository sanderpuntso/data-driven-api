﻿using System;
using System.Collections.Generic;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class Member : TenantEntity
    {
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public MemberType MemberType { get; set; }
    }
    public class GroupMember : Member
    {
        public Guid GroupId { get; set; }
        public virtual Group Group { get; set; }
        public virtual List<Session> Sessions { get; set; }
    }
    public class InstitutionMember : Member
    {
        public Guid InstitutionId { get; set; }
        public virtual Institution Institution { get; set; }
    }

    public class SessionMember : Member
    {
        public Guid SessionId { get; set; }
        public virtual Session Session { get; set; }
    }

    public enum MemberType
    {
        Admin,
        User
    }
}