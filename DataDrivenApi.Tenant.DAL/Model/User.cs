﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataDrivenApi.Tenant.DAL.Enums;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class User : TenantEntity
    {
        [Required, StringLength(100, MinimumLength = 5)]
        public string Email { get; set; }
        [Required, DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        [Required]
        public UserRole Role { get; set; }
        public virtual List<InstitutionMember> InstitutionMembers { get; set; }
        public virtual List<GroupMember> GroupMembers { get; set; }
        [InverseProperty("User")]
        public virtual List<Grade> Grades { get; set; }
        [InverseProperty("Grader")]
        public virtual List<Grade> Graded { get; set; }
    }
}