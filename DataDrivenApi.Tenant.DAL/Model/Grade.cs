﻿using System;
using System.ComponentModel.DataAnnotations;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class Grade : TenantEntity
    {
        [Required]
        public string Mark { get; set; }
        [Required]
        public DateTime Grading { get; set; } = DateTime.Now;
        public string Description { get; set; }
        public Guid AssignmentId { get; set; }
        public virtual Assignment Assignment { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public Guid GraderId { get; set; }
        public User Grader { get; set; }
    }
}