﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataDrivenApi.Tenant.DAL.Model.Shared;

namespace DataDrivenApi.Tenant.DAL.Model
{
    public class Group : TenantEntity
    {
        [Required]
        [StringLength(100, MinimumLength = 8)]
        public string Name { get; set; }
        public string Type { get; set; }
        [ForeignKey("Parent")]
        public Guid? ParentId { get; set; }
        public virtual Group Parent { get; set; }
        public virtual List<GroupMember> GroupMembers { get; set; }
        public virtual List<Group> Groups { get; set; }
        public Guid InstitutionId { get; set; }
        public virtual Institution Institution { get; set; }
        public virtual List<Session> Sessions { get; set; }
    }
}