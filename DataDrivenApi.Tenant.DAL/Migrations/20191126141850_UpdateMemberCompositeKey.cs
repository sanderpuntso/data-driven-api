﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataDrivenApi.Tenant.DAL.Migrations
{
    public partial class UpdateMemberCompositeKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sessions_GroupMembers_GroupMemberId",
                table: "Sessions");

            migrationBuilder.DropIndex(
                name: "IX_Sessions_GroupMemberId",
                table: "Sessions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SessionMembers",
                table: "SessionMembers");

            migrationBuilder.DropIndex(
                name: "IX_SessionMembers_SessionId",
                table: "SessionMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InstitutionMembers",
                table: "InstitutionMembers");

            migrationBuilder.DropIndex(
                name: "IX_InstitutionMembers_InstitutionId",
                table: "InstitutionMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers");

            migrationBuilder.DropIndex(
                name: "IX_GroupMembers_GroupId",
                table: "GroupMembers");

            migrationBuilder.DropColumn(
                name: "GroupMemberId",
                table: "Sessions");

            migrationBuilder.AddColumn<Guid>(
                name: "GroupMemberGroupId",
                table: "Sessions",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "GroupMemberUserId",
                table: "Sessions",
                nullable: true);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_SessionMembers_Id",
                table: "SessionMembers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SessionMembers",
                table: "SessionMembers",
                columns: new[] { "SessionId", "UserId" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_InstitutionMembers_Id",
                table: "InstitutionMembers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InstitutionMembers",
                table: "InstitutionMembers",
                columns: new[] { "InstitutionId", "UserId" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_GroupMembers_Id",
                table: "GroupMembers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers",
                columns: new[] { "GroupId", "UserId" });

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_GroupMemberGroupId_GroupMemberUserId",
                table: "Sessions",
                columns: new[] { "GroupMemberGroupId", "GroupMemberUserId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Sessions_GroupMembers_GroupMemberGroupId_GroupMemberUserId",
                table: "Sessions",
                columns: new[] { "GroupMemberGroupId", "GroupMemberUserId" },
                principalTable: "GroupMembers",
                principalColumns: new[] { "GroupId", "UserId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sessions_GroupMembers_GroupMemberGroupId_GroupMemberUserId",
                table: "Sessions");

            migrationBuilder.DropIndex(
                name: "IX_Sessions_GroupMemberGroupId_GroupMemberUserId",
                table: "Sessions");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_SessionMembers_Id",
                table: "SessionMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SessionMembers",
                table: "SessionMembers");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_InstitutionMembers_Id",
                table: "InstitutionMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InstitutionMembers",
                table: "InstitutionMembers");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_GroupMembers_Id",
                table: "GroupMembers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers");

            migrationBuilder.DropColumn(
                name: "GroupMemberGroupId",
                table: "Sessions");

            migrationBuilder.DropColumn(
                name: "GroupMemberUserId",
                table: "Sessions");

            migrationBuilder.AddColumn<Guid>(
                name: "GroupMemberId",
                table: "Sessions",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SessionMembers",
                table: "SessionMembers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InstitutionMembers",
                table: "InstitutionMembers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GroupMembers",
                table: "GroupMembers",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Sessions_GroupMemberId",
                table: "Sessions",
                column: "GroupMemberId");

            migrationBuilder.CreateIndex(
                name: "IX_SessionMembers_SessionId",
                table: "SessionMembers",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_InstitutionMembers_InstitutionId",
                table: "InstitutionMembers",
                column: "InstitutionId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMembers_GroupId",
                table: "GroupMembers",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sessions_GroupMembers_GroupMemberId",
                table: "Sessions",
                column: "GroupMemberId",
                principalTable: "GroupMembers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
