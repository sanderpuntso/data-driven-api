﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataDrivenApi.Tenant.DAL.Migrations
{
    public partial class UpdateMemberTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MemberType",
                table: "SessionMembers");

            migrationBuilder.DropColumn(
                name: "MemberType",
                table: "InstitutionMembers");

            migrationBuilder.DropColumn(
                name: "MemberType",
                table: "GroupMembers");

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "SessionMembers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "InstitutionMembers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Role",
                table: "GroupMembers",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "SessionMembers");

            migrationBuilder.DropColumn(
                name: "Role",
                table: "InstitutionMembers");

            migrationBuilder.DropColumn(
                name: "Role",
                table: "GroupMembers");

            migrationBuilder.AddColumn<int>(
                name: "MemberType",
                table: "SessionMembers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MemberType",
                table: "InstitutionMembers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MemberType",
                table: "GroupMembers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
