﻿using System.Runtime.Serialization;

namespace DataDrivenApi.Tenant.DAL.Enums
{
    public enum UserRole
    {
        [EnumMember(Value = "Admin")]
        Admin = 1,
        [EnumMember(Value = "Employee")]
        Employee = 2,
        [EnumMember(Value = "User")]
        User = 3
    }
}