﻿using System.IO;
using System.Linq;
using DataDrivenApi.DAL;
using DataDrivenApi.Tenant.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;

namespace DatabaseManager
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder.Build();

            using var dbContext = 
                new SharedDbContext(configuration.GetConnectionString("SharedDbContext"));

            dbContext.Database.Migrate();

            var connections = dbContext.Tenants.Select(t => t.ConnectionString).AsEnumerable();
            foreach (var connection in connections)
            {
                new TenantDbContext(connection).Migrate();
            }
        }
    }
}