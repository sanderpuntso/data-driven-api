## Database creation

This project is developed with code-first migrations. Solution has two central databases.
To create these databases, run following commands in Package Manager Console:

Update-Database -Context SharedDbContext
Update-Database -Context TenantDbContext
Update-Database -Context RegulationDbContext