﻿using System;
using DataDrivenApi.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Infrastructure.Internal;

namespace DataDrivenApi.DAL
{
    public class SharedDbContext : DbContext
    {
        private readonly string _connectionString;

        public SharedDbContext(DbContextOptions<SharedDbContext> options)
            : base(options)
        {
            _connectionString = options.GetExtension<SqlServerOptionsExtension>().ConnectionString;
        }

        public SharedDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlServer(_connectionString);
        }

        public DbSet<Tenant> Tenants { get; set; }


        public bool TrySaveChanges(out string exception)
        {
            exception = null;
            try
            {
                SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                exception = e.Message;
                return false;
            }
        }
    }
}