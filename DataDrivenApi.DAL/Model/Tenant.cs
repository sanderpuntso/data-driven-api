﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataDrivenApi.DAL.Model
{
    public class Tenant
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required, StringLength(100, MinimumLength = 5)]
        public string Email { get; set; }

        [Required, DataType(DataType.Password)]
        public string Password { get; set; }

        [Required, StringLength(100, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        public string ConnectionString { get; set; }

        [Required, StringLength(100, MinimumLength = 5)]
        public string HostName { get; set; }
    }
}
