﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.Tenant.DAL.Enums;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Service.Interface
{
    public interface IRoleService
    {
        UserRole? GetRole(Guid tenantId, Guid actorId);
        MemberType? GetMemberTypeInInstitution(Guid tenantId, Guid userId, Guid institutionId);
        MemberType? GetMemberTypeInGroup(Guid tenantId, Guid userId, Guid groupId);
        MemberType? GetMemberTypeInSession(Guid tenantId, Guid userId, Guid sessionId);
    }
}