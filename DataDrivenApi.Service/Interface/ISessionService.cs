﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Session;

namespace DataDrivenApi.Service.Interface
{
    public interface ISessionService
    {
        MessagePackEntity<object> Add(Guid tenantId, Guid actorId, SessionCreationModel model);
        MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid sessionId);
        MessagePackEntity<object> GetSessions(Guid tenantId, Guid actorId, SessionSearchOptions options);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid sessionId, SessionCreationModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid sessionId);
        MessagePackEntity<object> AddSessionMember(Guid tenantId, Guid actorId, SessionMemberCreationModel model);
        MessagePackEntity<object> GetSessionMember(Guid tenantId, Guid actorId, Guid sessionMemberId);
        MessagePackEntity<object> GetSessionMembers(Guid tenantId, Guid actorId, SessionMemberSearchOptions options);
        MessagePackEntity<object> UpdateSessionMember(Guid tenantId, Guid actorId, Guid sessionMemberId, SessionMemberCreationModel model);
        MessagePackEntity<object> DeleteSessionMember(Guid tenantId, Guid actorId, Guid sessionMemberId);
        
    }
}