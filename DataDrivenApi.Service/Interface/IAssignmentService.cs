﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Assignment;

namespace DataDrivenApi.Service.Interface
{
    public interface IAssignmentService
    {
        MessagePackEntity<object> Add(Guid tenantId, Guid actorId, AssignmentCreationModel model);
        MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid assignmentId);
        MessagePackEntity<object> GetAssignments(Guid tenantId, Guid actorId, AssignmentSearchOptions options);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid assignmentId, AssignmentCreationModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid assignmentId);
    }
}