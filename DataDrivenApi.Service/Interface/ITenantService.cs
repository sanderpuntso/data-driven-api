﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Tenant;

namespace DataDrivenApi.Service.Interface
{
    public interface ITenantService
    {
        MessagePackEntity<object> Authenticate(TenantAuthenticationModel model);
        MessagePackEntity<object> Add(TenantCreationModel model);
        MessagePackEntity<object> Get(Guid tenantId);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, TenantUpdateModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId);
    }
}