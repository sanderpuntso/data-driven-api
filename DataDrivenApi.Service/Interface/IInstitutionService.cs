﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Institution;

namespace DataDrivenApi.Service.Interface
{
    public interface IInstitutionService
    {
        MessagePackEntity<object> Add(Guid tenantId, Guid actorId, InstitutionCreationModel model);
        MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid institutionId);
        MessagePackEntity<object> GetInstitutions(Guid tenantId, Guid actorId, InstitutionSearchOptions options);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid institutionId, InstitutionCreationModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid institutionId);
        MessagePackEntity<object> AddInstitutionMember(Guid tenantId, Guid actorId,
            InstitutionMemberCreationModel model);
        MessagePackEntity<object> GetInstitutionMember(Guid tenantId, Guid actorId, Guid institutionMemberId);
        MessagePackEntity<object> GetInstitutionMembers(Guid tenantId, Guid actorId, InstitutionMemberSearchOptions options);
        MessagePackEntity<object> UpdateInstitutionMember(Guid tenantId, Guid actorId, Guid institutionMemberId,
            InstitutionMemberCreationModel model);
        MessagePackEntity<object> DeleteInstitutionMember(Guid tenantId, Guid actorId, Guid institutionMemberId);
    }
}