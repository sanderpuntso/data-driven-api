﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Group;

namespace DataDrivenApi.Service.Interface
{
    public interface IGroupService
    {
        MessagePackEntity<object> Add(Guid tenantId, Guid actorId, GroupCreationModel model);
        MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid groupId);
        MessagePackEntity<object> GetGroups(Guid tenantId, Guid actorId, GroupSearchOptions options);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid groupId, GroupCreationModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid groupId);
        MessagePackEntity<object> AddGroupMember(Guid tenantId, Guid actorId, GroupMemberCreationModel model);
        MessagePackEntity<object> GetGroupMember(Guid tenantId, Guid actorId, Guid groupMemberId);
        MessagePackEntity<object> GetGroupMembers(Guid tenantId, Guid actorId, GroupMemberSearchOptions options);
        MessagePackEntity<object> UpdateGroupMember(Guid tenantId, Guid actorId, Guid groupMemberId, GroupMemberCreationModel model);
        MessagePackEntity<object> DeleteGroupMember(Guid tenantId, Guid actorId, Guid groupMemberIdGuid);
    }
}