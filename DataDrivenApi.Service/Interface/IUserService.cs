﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.User;
using DataDrivenApi.Tenant.DAL.Enums;

namespace DataDrivenApi.Service.Interface
{
    public interface IUserService
    {
        MessagePackEntity<object> Authenticate(Guid tenantId, UserAuthenticationModel model);
        MessagePackEntity<object> Add(Guid tenantId, UserCreationModel model);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid userId, UserUpdateModel model);
        MessagePackEntity<object> AddUserRole(Guid tenantId, Guid actorId, UserRoleCreationModel model);
        MessagePackEntity<object> UpdateUserRole(Guid tenantId, Guid actorId, UserRoleCreationModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid userId);
        MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid userId);
        MessagePackEntity<object> GetUsers(Guid tenantId, Guid actorId, UserSearchOptions options);
    }
}