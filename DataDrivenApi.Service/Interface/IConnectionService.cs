﻿using System;
using DataDrivenApi.Tenant.DAL;

namespace DataDrivenApi.Service.Interface
{
    public interface IConnectionService
    {
        TenantDbContext Get(Guid tenantId);
    }
}