﻿using System;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Grade;

namespace DataDrivenApi.Service.Interface
{
    public interface IGradeService
    {
        MessagePackEntity<object> Add(Guid tenantId, Guid actorId, GradeCreationModel model);
        MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid gradeId);
        MessagePackEntity<object> GetGrades(Guid tenantId, Guid actorId, GradeSearchOptions options);
        MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid gradeId, GradeCreationModel model);
        MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid gradeId);

    }
}