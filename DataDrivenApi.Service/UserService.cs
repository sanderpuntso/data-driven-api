﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.User;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Enums;
using DataDrivenApi.Tenant.DAL.Model;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace DataDrivenApi.Service
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IConnectionService _connectionService;
        private readonly IMapper _mapper;
        private readonly IRoleService _roleService;

        public UserService(IOptionsMonitor<AppSettings> appSettings, IConnectionService connectionService, IMapper mapper, IRoleService roleService)
        {
            _connectionService = connectionService;
            _mapper = mapper;
            _roleService = roleService;
            _appSettings = appSettings.CurrentValue;
        }

        public MessagePackEntity<object> Authenticate(Guid tenantId, UserAuthenticationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var user =
                dbContext.Users.FirstOrDefault(x => x.Email == model.Email && x.Password == model.Password);

            if (user == null)
            {
                result.ErrorMessages.Add("User with email and password does not exist!");
                return result;
            }

            result.Result = new
            {
                token = GetUserToken(user, tenantId)
            };
            return result;
        }

        public MessagePackEntity<object> Add(Guid tenantId, UserCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var user = dbContext.Users.Add(new User
            {
                Email = model.Email,
                Password = model.Password,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                BirthDate = model.BirthDate,
                Role = UserRole.User
            });

            if (!dbContext.TrySaveChanges(out var dbException))
            {
                result.ErrorMessages.Add(dbException);
                return result;
            }
            
            result.Result = new
            {
                user = _mapper.Map<UserResponse>(user.Entity),
                token = GetUserToken(user.Entity, tenantId)
            };
            
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid userId, UserUpdateModel model)
        {
            var result = new MessagePackEntity<object>();
            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }
            
            var user = dbContext.Users.FirstOrDefault(u => u.Id == userId);

            if (user != null)
            {
                user.Email = model.Email;
                user.Password = model.Password;
                user.FirstName = model.FirstName;
                user.MiddleName = model.MiddleName;
                user.LastName = model.LastName;
                user.BirthDate = model.BirthDate;
                
                dbContext.Update(user);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }

                result.Result = new
                {
                    user = _mapper.Map<UserResponse>(user)
                };
                return result;
            }

            result.ErrorMessages.Add("No user found in database");
            return result;
        }

        public MessagePackEntity<object> AddUserRole(Guid tenantId, Guid actorId, UserRoleCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var user = dbContext.Users.FirstOrDefault(u => u.Id == model.UserId);

            if (user != null)
            {
                user.Role = model.Role;
                dbContext.Update(user);
                dbContext.SaveChanges();

                result.Result = new
                {
                    user = _mapper.Map<UserResponse>(user)
                };
                return result;
            }

            result.ErrorMessages.Add("No user found in database");
            return result;
        }

        public MessagePackEntity<object> UpdateUserRole(Guid tenantId, Guid actorId, UserRoleCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var user = dbContext.Users.FirstOrDefault(u => u.Id == model.UserId);

            if (user != null)
            {
                user.Role = model.Role;
                dbContext.Update(user);
                dbContext.SaveChanges();

                result.Result = new
                {
                    user = _mapper.Map<UserResponse>(user)
                };
                return result;
            }

            result.ErrorMessages.Add("No user found in database");
            return result;
        }

        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid userId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var user = dbContext.Users.FirstOrDefault(u => u.Id == userId);

            if (user != null)
            {
                dbContext.Users.Remove(user);
                dbContext.SaveChanges();

                result.Result = new
                {
                    user = user.Id
                };
                return result;
            }

            result.ErrorMessages.Add("No user found in database");
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid userId)
        {
            var result = new MessagePackEntity<object>();
            var role = _roleService.GetRole(tenantId, actorId);

            if (actorId != userId && role != UserRole.Admin)
            {
                result.ErrorMessages.Add("Accessing entity forbidden!");
                return result;
            }

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var user = dbContext.Users.FirstOrDefault(u => u.Id == userId);

            result.Result = new
            {
                user = _mapper.Map<UserResponse>(user)
            };
            return result;
        }

        public MessagePackEntity<object> GetUsers(Guid tenantId, Guid actorId, UserSearchOptions options)
        {
            var result = new MessagePackEntity<object>();
            //var role = _roleService.GetRole(tenantId, actorId);
            //if (role != null && role != UserRole.Admin)
            //{
            //    result.ErrorMessages.Add("Accessing entity forbidden!");
            //    return result;
            //}

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            //TODO: search options implementation
            var users = dbContext.Users.AsQueryable();
            //.Where(u => options.Email == u.Email && u.FirstName?.Contains(options.FirstName)).ToList();

            result.Result = new
            {
                users = _mapper.Map<List<UserResponse>>(users),
            };
            return result;
        }

        private object GetUserToken(User user, Guid tenantId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, tenantId.ToString()),
                    new Claim(ClaimTypes.Actor, user.Id.ToString()), 
                    new Claim(ClaimTypes.Role, user.Role.ToString())
                }),
                
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}