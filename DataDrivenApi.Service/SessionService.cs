﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Member;
using DataDrivenApi.DTO.Model.Session;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Service
{
    public class SessionService : ISessionService
    {
        private readonly IMapper _mapper;
        private readonly IConnectionService _connectionService;

        public SessionService(IConnectionService connectionService, IMapper mapper)
        {
            _connectionService = connectionService;
            _mapper = mapper;
        }

        public MessagePackEntity<object> Add(Guid tenantId, Guid actorId, SessionCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var session = dbContext.Sessions.Add(new Session
            {
                Topic = model.Topic,
                Subject = model.Subject,
                Time = model.Time,
                GroupId = model.GroupId,
                Place = model.Place
            });

            dbContext.SessionMembers.Add(new SessionMember
            {
                SessionId = session.Entity.Id,
                UserId = actorId,
                MemberType = MemberType.Admin
            });

            var group = dbContext.Groups.FirstOrDefault(g => g.Id == session.Entity.GroupId);

            group?.GroupMembers.ForEach(mem =>
            {
                if(session.Entity.SessionMembers.All(m => m.UserId != mem.UserId))
                    AddSessionMember(tenantId, actorId, new SessionMemberCreationModel
                        {
                            SessionId = session.Entity.Id,
                            MemberType = MemberType.User,
                            UserId = mem.UserId
                        });
            });

            if (!dbContext.TrySaveChanges(out var dbException))
            {
                result.ErrorMessages.Add(dbException);
                return result;
            }

            result.Result = new
            {
                session = _mapper.Map<SessionResponse>(session.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid sessionId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var session = dbContext.Sessions.FirstOrDefault(g => g.Id == sessionId);

            result.Result = new
            {
                session = _mapper.Map<SessionResponse>(session)
            };
            return result;
        }

        public MessagePackEntity<object> GetSessions(Guid tenantId, Guid actorId, SessionSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var sessions = dbContext.Sessions.AsQueryable();

            if (options.SessionId != null)
                sessions = sessions.Where(s => s.Id == options.SessionId);
            if (options.Topic != null)
                sessions = sessions.Where(s => s.Topic == options.Topic);
            if (options.Subject != null)
                sessions = sessions.Where(s => s.Subject == options.Subject);
            if (options.Place != null)
                sessions = sessions.Where(s => s.Place == options.Place);
            if (options.Time != null)
                sessions = sessions.Where(s => s.Time == options.Time);
            if (options.GroupId != null)
                sessions = sessions.Where(s => s.Id == options.GroupId);
            if (options.UserId != null)
                sessions = options.MemberType != null
                    ? sessions.Where(s => s.SessionMembers.Any(sm => sm.UserId == options.UserId
                                                                     && sm.MemberType == options.MemberType))
                    : sessions.Where(s => s.SessionMembers.Any(sm => sm.UserId == options.UserId));

            result.Result = new
            {
                sessions = _mapper.Map<List<SessionResponse>>(sessions)
            };
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid sessionId, SessionCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var session = dbContext.Sessions.FirstOrDefault(s => s.Id == sessionId);

            if (session != null)
            {
                session.Time = model.Time;
                session.Place = model.Place;
                session.GroupId = model.GroupId;
                dbContext.Update(session);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }

            result.Result = new
            {
                session = _mapper.Map<SessionResponse>(session)
            };
            return result;
        }
        
        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid sessionId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var session = dbContext.Sessions.FirstOrDefault(s => s.Id == sessionId);

            if (session != null)
            {
                dbContext.Sessions.Remove(session);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }

            result.Result = new
            {
                session = session?.Id
            };
            return result;
        }

        public MessagePackEntity<object> AddSessionMember(Guid tenantId, Guid actorId, SessionMemberCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var sessionMember = dbContext.SessionMembers.Add(new SessionMember
            {
                MemberType = model.MemberType,
                SessionId = model.SessionId,
                UserId = model.UserId
            });

            if (!dbContext.TrySaveChanges(out var dbException))
            {
                result.ErrorMessages.Add(dbException);
                return result;
            }

            result.Result = new
            {
                member = _mapper.Map<SessionMemberResponse>(sessionMember.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> GetSessionMember(Guid tenantId, Guid actorId, Guid sessionMemberId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var sessionMember = dbContext.SessionMembers.FirstOrDefault(i => i.Id == sessionMemberId);

            result.Result = new
            {
                member = _mapper.Map<SessionMemberResponse>(sessionMember)
            };
            return result;
        }

        public MessagePackEntity<object> GetSessionMembers(Guid tenantId, Guid actorId, SessionMemberSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var sessionMembers = dbContext.SessionMembers.AsQueryable();

            if (options.SessionMemberId != null)
                sessionMembers = sessionMembers.Where(sm => sm.Id == options.SessionMemberId);
            if (options.UserId != null)
                sessionMembers = sessionMembers.Where(sm => sm.UserId == options.UserId);
            if (options.MemberType != null)
                sessionMembers = sessionMembers.Where(sm => sm.MemberType == options.MemberType);

            result.Result = new
            {
                members = _mapper.Map<List<SessionMember>>(sessionMembers)
            };
            return result;
        }

        public MessagePackEntity<object> UpdateSessionMember(Guid tenantId, Guid actorId, Guid sessionMemberId,
            SessionMemberCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var sessionMember = dbContext.SessionMembers.FirstOrDefault(m => m.Id == sessionMemberId);

            if (sessionMember != null)
            {
                sessionMember.SessionId = model.SessionId;
                sessionMember.UserId = model.UserId;
                sessionMember.MemberType = model.MemberType;
                dbContext.Update(sessionMember);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }

            result.Result = new
            {
                member = _mapper.Map<SessionMemberResponse>(sessionMember)
            };
            return result;
        }

        public MessagePackEntity<object> DeleteSessionMember(Guid tenantId, Guid actorId, Guid sessionMemberId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var sessionMember = dbContext.SessionMembers.FirstOrDefault(m => m.Id == sessionMemberId);

            if (sessionMember != null)
            {
                dbContext.SessionMembers.Remove(sessionMember);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }

            result.Result = new
            {
                member = sessionMember?.Id
            };
            return result;
        }
    }
}