﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Institution;
using DataDrivenApi.DTO.Model.Member;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL;
using DataDrivenApi.Tenant.DAL.Model;
using Microsoft.EntityFrameworkCore;

namespace DataDrivenApi.Service
{
    public class InstitutionService : IInstitutionService
    {
        private readonly IConnectionService _connectionService;
        private readonly IMapper _mapper;

        public InstitutionService(IConnectionService connectionService, IMapper mapper)
        {
            _connectionService = connectionService;
            _mapper = mapper;
        }

        public MessagePackEntity<object> Add(Guid tenantId, Guid actorId, InstitutionCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institution = dbContext.Institutions.Add(new Institution
            {
                Name = model.Name,
                Type = model.Type
            });

            dbContext.InstitutionMembers.Add(new InstitutionMember
            {
                MemberType = MemberType.Admin,
                UserId = actorId,
                InstitutionId = institution.Entity.Id
            });

            if (!dbContext.TrySaveChanges(out var dbException))
            {
                result.ErrorMessages.Add(dbException);
                return result;
            }

            result.Result = new
            {
                institution = _mapper.Map<InstitutionResponse>(institution.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid institutionId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institution = dbContext.Institutions.FirstOrDefault(i => i.Id == institutionId);

            result.Result = new
            {
                institution = _mapper.Map<InstitutionResponse>(institution)
            };
            return result;
        }

        public MessagePackEntity<object> GetInstitutions(Guid tenantId, Guid actorId, InstitutionSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institutions = dbContext.Institutions.AsEnumerable();

            if (options.InstitutionId != null)
                institutions = institutions.Where(i => i.Id == options.InstitutionId);
            if (options.Name != null)
                institutions = institutions.Where(i => i.Name.Contains(options.Name));
            if (options.Type != null)
                institutions = institutions.Where(i => i.Type == options.Type);
            options.Name = "Puurmani Keskkool";
            var name = options.Name;
            object[] list = new object[] {"Puurmani Keskkool", "Puurmani Põhikool"};

            var vm = options.GetType().GetProperties().Where(p => p.Name != "props").ToList();

            var omo = options.GetType().GetProperties().Select(p => p.Name).ToList();
            var optionValues = options.GetType().GetProperties()
                .Where(p => p.Name != "Props")
                .Select(p => p.GetValue(options, null)).ToList();

            var props = options.GetProps();

            var x = dbContext.Institutions
                .FromSqlRaw("SELECT Id, Type as Name, Type, IsDeleted, Name as Test FROM Institutions WHERE Name = {1}", props)
                //.Where(x => x.Name == "test")
                .ToList();


            //var XC = dbContext.Database
            //    .SqlQuery<>("SELECT * from Institutions");

            //var mm = dbContext.Institutions
            //    .FromSqlRaw(
            //        "select a.*, m.MemberType from Institutions a left join InstitutionMembers m on a.Id = m.InstitutionId where m.MemberType = 0").ToList();

            //var ssss = 1;

            expressionFunc();
            //ParameterExpression t = Expression.Parameter(typeof(InstitutionResponse), "t");

            //var prop = "Name";
            //var comp = "Contains";

            //Expression l = Expression.Constant(t);

            //var tt = Expression.Property(t, "Name");
            //MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            //var someValue = Expression.Constant("Puurmani Põhikool", typeof(string));
            //var e = Expression.Equal(tt, /*method ?? throw new InvalidOperationException(),*/ someValue);

            //Expression predicateBody = Expression.OrElse(e, e);

            //// Create an expression tree that represents the expression  
            //// 'queryableData.Where(company => (company.ToLower() == "coho winery" || company.Length > 16))'  
            //MethodCallExpression whereCallExpression = Expression.Call(
            //    typeof(IQueryable<InstitutionResponse>),
            //    "Where",
            //    new Type[] { x.ElementType },
            //    x.Expression,
            //    Expression.Lambda<Func<InstitutionResponse, bool>>(predicateBody, new ParameterExpression[] { t }));
            //// ***** End Where *****  

            //// Create an executable query from the expression tree.  
            //IQueryable<InstitutionResponse> results = x.Provider.CreateQuery<InstitutionResponse>(whereCallExpression);
            //var test = results.ToList();

            result.Result = new
            {
                institutions = x
                    //_mapper.Map<List<InstitutionResponse>>(institutions)
            };
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid institutionId, InstitutionCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institution = dbContext.Institutions.FirstOrDefault(i => i.Id == institutionId);

            if (institution != null)
            {
                institution.Name = model.Name;
                institution.Type = model.Type;
                dbContext.Update(institution);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }
            
            result.Result = new
            {
                institution = _mapper.Map<InstitutionResponse>(institution)
            };
            return result;
        }

        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid institutionId)
        {var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institution = dbContext.Institutions.FirstOrDefault(i => i.Id == institutionId);

            if (institution != null)
            {
                dbContext.Institutions.Remove(institution);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }
            
            result.Result = new
            {
                institution = institution?.Id
            };
            return result;
        }

        public MessagePackEntity<object> AddInstitutionMember(Guid tenantId, Guid actorId,
            InstitutionMemberCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institutionMember = dbContext.InstitutionMembers.Add(new InstitutionMember
            {
                MemberType = model.MemberType,
                InstitutionId = model.InstitutionId,
                UserId = model.UserId
            });

            if (!dbContext.TrySaveChanges(out var dbException))
            {
                result.ErrorMessages.Add(dbException);
                return result;
            }

            result.Result = new
            {
                member = _mapper.Map<InstitutionMemberResponse>(institutionMember.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> GetInstitutionMember(Guid tenantId, Guid actorId, Guid institutionMemberId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institutionMember = dbContext.InstitutionMembers.FirstOrDefault(i => i.Id == institutionMemberId);

            result.Result = new
            {
                member = _mapper.Map<InstitutionMemberResponse>(institutionMember)
            };
            return result;
        }

        public MessagePackEntity<object> GetInstitutionMembers(Guid tenantId, Guid actorId,
            InstitutionMemberSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institutionMembers = dbContext.InstitutionMembers.AsQueryable();

            if (options.InstitutionMemberId != null)
                institutionMembers = institutionMembers.Where(im => im.Id == options.InstitutionMemberId);
            if (options.UserId != null)
                institutionMembers = institutionMembers.Where(im => im.UserId == options.UserId);
            if (options.MemberType != null)
                institutionMembers = institutionMembers.Where(im => im.MemberType == options.MemberType);

            result.Result = new
            {
                institutionMembers
            };
            return result;
        }

        public MessagePackEntity<object> UpdateInstitutionMember(Guid tenantId, Guid actorId, Guid institutionMemberId,
            InstitutionMemberCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institutionMember = dbContext.InstitutionMembers.FirstOrDefault(m => m.Id == institutionMemberId);

            if (institutionMember != null)
            {
                institutionMember.InstitutionId = model.InstitutionId;
                institutionMember.UserId = model.UserId;
                institutionMember.MemberType = model.MemberType;
                dbContext.InstitutionMembers.Update(institutionMember);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }

            result.Result = new
            {
                member = _mapper.Map<InstitutionMemberResponse>(institutionMember)
            };
            return result;
        }

        public MessagePackEntity<object> DeleteInstitutionMember(Guid tenantId, Guid actorId, Guid institutionMemberId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var institutionMember = dbContext.InstitutionMembers.FirstOrDefault(m => m.Id == institutionMemberId);

            if (institutionMember != null)
            {
                dbContext.InstitutionMembers.Remove(institutionMember);

                if (!dbContext.TrySaveChanges(out var dbException))
                {
                    result.ErrorMessages.Add(dbException);
                    return result;
                }
            }

            result.Result = new
            {
                member = _mapper.Map<InstitutionMemberResponse>(institutionMember)
            };
            return result;
        }




        public void expressionFunc()
        {
            string[] companies = { "Consolidated Messenger", "Alpine Ski House", "Southridge Video", "City Power & Light",
                "Coho Winery", "Wide World Importers", "Graphic Design Institute", "Adventure Works",
                "Humongous Insurance", "Woodgrove Bank", "Margie's Travel", "Northwind Traders",
                "Blue Yonder Airlines", "Trey Research", "The Phone Company",
                "Wingtip Toys", "Lucerne Publishing", "Fourth Coffee" };

            IQueryable<String> queryableData = companies.AsQueryable<string>();

            ParameterExpression pe = Expression.Parameter(typeof(string), "company");

            Expression left = Expression.Convert(pe, typeof(string));
            Expression right = Expression.Constant("Coho winery");
            Expression expression = Expression.Equal(left, right);

            MethodCallExpression whereCallExpression = Expression.Call(
                typeof(Queryable),
                "Where",
                new Type[] { queryableData.ElementType },
                queryableData.Expression,
                expression);

            IQueryable<string> results = queryableData.Provider.CreateQuery<string>(whereCallExpression);

        }
    }
}