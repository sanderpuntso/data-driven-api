﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Group;
using DataDrivenApi.DTO.Model.Member;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Service
{
    public class GroupService : IGroupService
    {
        private readonly IConnectionService _connectionService;
        private readonly IMapper _mapper;

        public GroupService(IConnectionService connectionService, IMapper mapper)
        {
            _connectionService = connectionService;
            _mapper = mapper;
        }

        public MessagePackEntity<object> Add(Guid tenantId, Guid actorId, GroupCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var group = dbContext.Groups.Add(new Group
            {
                Name = model.Name,
                Type = model.Type,
                InstitutionId = model.InstitutionId,
                ParentId = model.ParentId
            });

            dbContext.GroupMembers.Add(new GroupMember
            {
                GroupId = group.Entity.Id,
                UserId = actorId,
                MemberType = MemberType.Admin
            });

            if (!dbContext.TrySaveChanges(out var dbException))
            {
                result.ErrorMessages.Add(dbException);
                return result;
            }

            result.Result = new
            {
                group = _mapper.Map<GroupResponse>(group.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid groupId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var group = dbContext.Groups.FirstOrDefault(g => g.Id == groupId);

            result.Result = new
            {
                group = _mapper.Map<GroupResponse>(group)
            };
            return result;
        }

        public MessagePackEntity<object> GetGroups(Guid tenantId, Guid actorId, GroupSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var groups = dbContext.Groups.AsQueryable();

            if (options.GroupId != null)
                groups = groups.Where(g => g.Id == options.GroupId);
            if (options.Name != null)
                groups = groups.Where(g => g.Name == options.Name);
            if (options.Type != null)
                groups = groups.Where(g => g.Type == options.Type);
            if (options.InstitutionId != null)
                groups = groups.Where(g => g.InstitutionId == options.InstitutionId);
            if (options.ParentGroupId != null)
                groups = groups.Where(g => g.ParentId == options.ParentGroupId);
            if (options.UserId != null)
                groups = options.MemberType != null
                    ? groups.Where(g => g.GroupMembers.Any(gm =>
                        gm.UserId == options.UserId && gm.MemberType == options.MemberType))
                    : groups.Where(g => g.GroupMembers.Any(gm =>
                        gm.UserId == options.UserId));

            result.Result = new
            {
                groups = _mapper.Map<List<GroupResponse>>(groups)
            };
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid groupId, GroupCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var group = dbContext.Groups.FirstOrDefault(g => g.Id == groupId);

            if (group != null)
            {
                group.Name = model.Name;
                group.Type = model.Type;
                group.InstitutionId = model.InstitutionId;
                group.ParentId = model.ParentId;
                dbContext.Update(group);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                group = _mapper.Map<GroupResponse>(group)
            };
            return result;
        }

        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid groupId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var group = dbContext.Groups.FirstOrDefault(g => g.Id == groupId);

            if (group != null)
            {
                dbContext.Groups.Remove(group);
                dbContext.SaveChanges();
            }
            
            result.Result = new
            {
                group = group?.Id
            };
            return result;
        }

        public MessagePackEntity<object> AddGroupMember(Guid tenantId, Guid actorId, GroupMemberCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var groupMember = dbContext.GroupMembers.Add(new GroupMember
            {
                MemberType = model.MemberType,
                GroupId = model.GroupId,
                UserId = model.UserId
            });

            result.Result = new
            {
                member = _mapper.Map<GroupMemberResponse>(groupMember.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> GetGroupMember(Guid tenantId, Guid actorId, Guid groupMemberId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var groupMember = dbContext.GroupMembers.FirstOrDefault(i => i.Id == groupMemberId);

            result.Result = new
            {
                member = _mapper.Map<GroupMemberResponse>(groupMember)
            };
            return result;
        }

        public MessagePackEntity<object> GetGroupMembers(Guid tenantId, Guid actorId, GroupMemberSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var groupMembers = dbContext.GroupMembers.AsQueryable();

            if (options.GroupMemberId != null)
                groupMembers = groupMembers.Where(gm => gm.Id == options.GroupMemberId);
            if (options.UserId != null)
                groupMembers = groupMembers.Where(gm => gm.UserId == options.UserId);
            if (options.MemberType != null)
                groupMembers = groupMembers.Where(gm => gm.MemberType == options.MemberType);

            result.Result = new
            {
                members = _mapper.Map<List<GroupMemberResponse>>(groupMembers)
            };
            return result;
        }

        public MessagePackEntity<object> UpdateGroupMember(Guid tenantId, Guid actorId, Guid groupMemberId, GroupMemberCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var groupMember = dbContext.GroupMembers.FirstOrDefault(m => m.Id == groupMemberId);

            if (groupMember != null)
            {
                groupMember.GroupId = model.GroupId;
                groupMember.UserId = model.UserId;
                groupMember.MemberType = model.MemberType;
                dbContext.Update(groupMember);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                member = _mapper.Map<GroupMemberResponse>(groupMember)
            };
            return result;
        }

        public MessagePackEntity<object> DeleteGroupMember(Guid tenantId, Guid actorId, Guid groupMemberId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var groupMember = dbContext.GroupMembers.FirstOrDefault(m => m.Id == groupMemberId);

            if (groupMember != null)
            {
                dbContext.GroupMembers.Remove(groupMember);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                member = groupMember?.Id
            };
            return result;
        }
    }
}