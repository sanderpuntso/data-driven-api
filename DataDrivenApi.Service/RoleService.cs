﻿using System;
using System.Linq;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Enums;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Service
{
    public class RoleService : IRoleService
    {
        private readonly IConnectionService _connectionService;

        public RoleService(IConnectionService connectionService)
        {
            _connectionService = connectionService;
        }

        public UserRole? GetRole(Guid tenantId, Guid actorId)
        {
            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                throw new UnauthorizedAccessException();
            }

            return dbContext.Users.FirstOrDefault(u => u.Id == actorId)?.Role;
        }

        public MemberType? GetMemberTypeInInstitution(Guid tenantId, Guid userId, Guid institutionId)
        {
            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                throw new UnauthorizedAccessException();
            }

            return dbContext.Institutions.FirstOrDefault(i => i.Id == institutionId)?
                .InstitutionMembers.FirstOrDefault(m => m.UserId == userId)?.MemberType;
        }

        public MemberType? GetMemberTypeInGroup(Guid tenantId, Guid userId, Guid groupId)
        {
            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                throw new UnauthorizedAccessException();
            }

            return dbContext.Groups.FirstOrDefault(i => i.Id == groupId)?
                .GroupMembers.FirstOrDefault(m => m.UserId == userId)?.MemberType;
        }

        public MemberType? GetMemberTypeInSession(Guid tenantId, Guid userId, Guid sessionId)
        {
            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                throw new UnauthorizedAccessException();
            }

            return dbContext.Sessions.FirstOrDefault(i => i.Id == sessionId)?
                .SessionMembers.FirstOrDefault(m => m.UserId == userId)?.MemberType;
        }
    }
}