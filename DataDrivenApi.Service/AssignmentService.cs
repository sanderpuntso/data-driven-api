﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Assignment;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Service
{
    public class AssignmentService : IAssignmentService
    {
        private readonly IConnectionService _connectionService;
        private readonly IMapper _mapper;

        public AssignmentService(IMapper mapper, IConnectionService connectionService)
        {
            _mapper = mapper;
            _connectionService = connectionService;
        }

        public MessagePackEntity<object> Add(Guid tenantId, Guid actorId, AssignmentCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var assignment = dbContext.Assignments.Add(new Assignment
            {
                SessionId = model.SessionId,
                Description = model.Description,
                DueDate = model.DueDate
            });

            dbContext.SaveChanges();

            result.Result = new
            {
                assignment = _mapper.Map<AssignmentResponse>(assignment.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid assignmentId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var assignment = dbContext.Assignments.FirstOrDefault(a => a.Id == assignmentId);

            result.Result = new
            {
                assignment = _mapper.Map<AssignmentResponse>(assignment)
            };
            return result;
        }

        public MessagePackEntity<object> GetAssignments(Guid tenantId, Guid actorId, AssignmentSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var assignments = dbContext.Assignments.AsEnumerable();

            if (options.Description != null)
                assignments = assignments.Where(a => a.Description.Contains(options.Description));
            if (options.DueDate != null)
                assignments = assignments.Where(a => a.DueDate == options.DueDate);
            if (options.SessionId != null)
                assignments = assignments.Where(a => a.SessionId == options.SessionId);
            if (options.UserId != null)
                assignments = options.MemberType != null
                    ? assignments.Where(a => a.Session.SessionMembers.Any(sm => sm.UserId == options.UserId &&
                                                                                sm.MemberType == options.MemberType))
                    : assignments.Where(a => a.Session.SessionMembers.Any(sm => sm.UserId == options.UserId));

            result.Result = new
            {
                assignments = _mapper.Map<List<AssignmentResponse>>(assignments)
            };
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid assignmentId, AssignmentCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var assignment = dbContext.Assignments.FirstOrDefault(a => a.Id == assignmentId);

            if (assignment != null)
            {
                assignment.Description = model.Description;
                assignment.SessionId = model.SessionId;
                assignment.DueDate = model.DueDate;
                dbContext.Update(assignment);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                assignment = _mapper.Map<AssignmentResponse>(assignment)
            };
            return result;
        }

        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid assignmentId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var assignment = dbContext.Assignments.FirstOrDefault(a => a.Id == assignmentId);

            if (assignment != null)
            {
                dbContext.Assignments.Remove(assignment);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                deletedAssignmentId = assignment?.Id
            };
            return result;
        }

    }
}