﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Grade;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL.Model;

namespace DataDrivenApi.Service
{
    public class GradeService : IGradeService
    {
        private readonly IConnectionService _connectionService;
        private readonly IMapper _mapper;

        public GradeService(IConnectionService connectionService, IMapper mapper)
        {
            _connectionService = connectionService;
            _mapper = mapper;
        }

        public MessagePackEntity<object> Add(Guid tenantId, Guid actorId, GradeCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var grade = dbContext.Grades.Add(new Grade
            {
                UserId = model.UserId,
                AssignmentId = model.AssignmentId,
                GraderId = model.GraderId,
                Mark = model.Mark
            });

            dbContext.SaveChanges();

            result.Result = new
            {
                grade = _mapper.Map<GradeResponse>(grade.Entity)
            };
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId, Guid actorId, Guid gradeId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var grade = dbContext.Grades.FirstOrDefault(g => g.Id == gradeId);

            result.Result = new
            {
                grade = _mapper.Map<GradeResponse>(grade)
            };
            return result;
        }

        public MessagePackEntity<object> GetGrades(Guid tenantId, Guid actorId, GradeSearchOptions options)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var grades = dbContext.Grades.AsEnumerable();

            if (options.Description != null)
                grades = grades.Where(g => g.Description.Contains(options.Description));
            if (options.Mark != null)
                grades = grades.Where(g => g.Mark == options.Mark);
            if (options.Grading != null)
                grades = grades.Where(g => g.Grading == options.Grading);
            if (options.AssignmentId != null)
                grades = grades.Where(g => g.AssignmentId == options.AssignmentId);
            if (options.UserId != null)
                grades = grades.Where(g => g.UserId == options.UserId);
            if (options.GraderId != null)
                grades = grades.Where(g => g.GraderId == options.GraderId);

            result.Result = new
            {
                grades = _mapper.Map<List<GradeResponse>>(grades)
            };
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, Guid gradeId, GradeCreationModel model)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var grade = dbContext.Grades.FirstOrDefault(g => g.Id == gradeId);

            if (grade != null)
            {
                grade.Mark = model.Mark;
                grade.Description = model.Description;
                grade.UserId = model.UserId;
                grade.GraderId = model.GraderId;
                grade.AssignmentId = model.AssignmentId;
                dbContext.Update(grade);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                grade = _mapper.Map<GradeResponse>(grade)
            };
            return result;
        }
        
        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId, Guid gradeId)
        {
            var result = new MessagePackEntity<object>();

            using var dbContext = _connectionService.Get(tenantId);
            if (dbContext == null)
            {
                result.ErrorMessages.Add("Cannot create tenant database connection!");
                return result;
            }

            var grade = dbContext.Grades.FirstOrDefault(g => g.Id == gradeId);

            if (grade != null)
            {
                dbContext.Grades.Remove(grade);
                dbContext.SaveChanges();
            }

            result.Result = new
            {
                grade = _mapper.Map<GradeResponse>(grade)
            };
            return result;
        }
    }
}