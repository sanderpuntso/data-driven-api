﻿using System;
using System.Linq;
using DataDrivenApi.DAL;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL;

namespace DataDrivenApi.Service
{
    public class ConnectionService : IConnectionService
    {
        private readonly SharedDbContext _sharedDbContext;

        public ConnectionService(SharedDbContext sharedDbContext)
        {
            _sharedDbContext = sharedDbContext;
        }

        public TenantDbContext Get(Guid tenantId)
        {
            var tenant =
                _sharedDbContext.Tenants.FirstOrDefault(t => t.Id == tenantId);
            return
                tenant?.ConnectionString != null ? new TenantDbContext(tenant.ConnectionString) : null;
        }
    }
}