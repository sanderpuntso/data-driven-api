﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using DataDrivenApi.Common;
using DataDrivenApi.DAL;
using DataDrivenApi.DTO.Model;
using DataDrivenApi.DTO.Model.Tenant;
using DataDrivenApi.DTO.Model.User;
using DataDrivenApi.Service.Interface;
using DataDrivenApi.Tenant.DAL;
using DataDrivenApi.Tenant.DAL.Enums;
using DataDrivenApi.Tenant.DAL.Model;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace DataDrivenApi.Service
{
    public class TenantService : ITenantService
    {
        private readonly AppSettings _appSettings;
        private readonly SharedDbContext _sharedDbContext;
        private readonly IMapper _mapper;

        public TenantService(SharedDbContext sharedDbContext, IOptionsMonitor<AppSettings> appSettings, IMapper mapper)
        {
            _sharedDbContext = sharedDbContext;
            _mapper = mapper;
            _appSettings = appSettings.CurrentValue;
        }

        public MessagePackEntity<object> Authenticate(TenantAuthenticationModel model)
        {
            var result = new MessagePackEntity<object>();
            var tenant =
                _sharedDbContext.Tenants.FirstOrDefault(x => x.Email == model.Email && x.Password == model.Password);

            if (tenant == null)
            {
                result.ErrorMessages.Add("Tenant with email and password does not exist!");
                return result;
            }

            result.Result = new
            {
                token = GetTenantToken(tenant)
            };
            return result;
        }

        public MessagePackEntity<object> Add(TenantCreationModel model)
        {
            var result = new MessagePackEntity<object>();
            var tenant = _sharedDbContext.Tenants.FirstOrDefault(x => x.Email == model.Email);

            if (tenant != null)
            {
                result.ErrorMessages.Add("Tenant with email already exists!");
                return result;
            }

            tenant = new DAL.Model.Tenant
            {
                Email = model.Email,
                Password = model.Password,
                HostName = model.HostName,
                Name = model.Name
            };

            _sharedDbContext.Tenants.Add(tenant);

            tenant.ConnectionString =
                string.Format(Constants.ConnectionString, tenant.Id);

            if (!_sharedDbContext.TrySaveChanges(out var ex))
            {
                result.ErrorMessages.Add(ex);
                return result;
            }

            var tenantDbContext = new TenantDbContext(tenant.ConnectionString);
            tenantDbContext.Migrate();

            var user = tenantDbContext.Users.Add(new User
            {
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                Email = tenant.Email,
                Password = tenant.Password,
                Role = UserRole.Admin
            });

            if (!tenantDbContext.TrySaveChanges(out var exception))
            {
                result.ErrorMessages.Add(exception);
                return result;
            }

            result.Result = new
            {
                tenant = _mapper.Map<TenantResponse>(tenant),
                user = _mapper.Map<UserResponse>(user.Entity),
                token = GetTenantToken(tenant)
            };
            return result;
        }

        public MessagePackEntity<object> Get(Guid tenantId)
        {
            var result = new MessagePackEntity<object>();
            var tenant = _sharedDbContext.Tenants.FirstOrDefault(x => x.Id == tenantId);

            if (tenant == null)
            {
                result.ErrorMessages.Add("No tenant in database!");
                return result;
            }

            result.Result = new
            {
                tenant = _mapper.Map<TenantResponse>(tenant)
            };
            return result;
        }

        public MessagePackEntity<object> Update(Guid tenantId, Guid actorId, TenantUpdateModel model)
        {
            var result = new MessagePackEntity<object>();
            var tenant = _sharedDbContext.Tenants.FirstOrDefault(x => x.Id == tenantId);

            if (tenant == null)
            {
                result.ErrorMessages.Add("No tenant in database!");
                return result;
            }

            tenant.Email = model.Email;
            tenant.HostName = model.HostName;
            tenant.Name = model.Name;
            tenant.Password = model.Password;

            _sharedDbContext.Tenants.Update(tenant);

            if (!_sharedDbContext.TrySaveChanges(out var exception))
            {
                result.ErrorMessages.Add(exception);
                return result;
            }

            result.Result = new
            {
                tenant = _mapper.Map<TenantResponse>(tenant)
            };
            return result;
        }

        public MessagePackEntity<object> Delete(Guid tenantId, Guid actorId)
        {
            var result = new MessagePackEntity<object>();
            var tenant = _sharedDbContext.Tenants.FirstOrDefault(x => x.Id == tenantId);

            if (tenant == null)
            {
                result.ErrorMessages.Add("No tenant in database!");
                return result;
            }

            _sharedDbContext.Tenants.Remove(tenant);

            if (!_sharedDbContext.TrySaveChanges(out var exception))
            {
                result.ErrorMessages.Add(exception);
                return result;
            }

            result.Result = new
            {
                tenant = tenant.Id
            };
            return result;
        }

        private string GetTenantToken(DAL.Model.Tenant tenant)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, tenant.Id.ToString()),
                    new Claim(ClaimTypes.Name, tenant.Name) 
                }),
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}