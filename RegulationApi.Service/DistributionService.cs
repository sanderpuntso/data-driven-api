﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Aq.ExpressionJsonSerializer;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RegulationApi.DAL;
using RegulationApi.DAL.Model;
using RegulationApi.Service.Interface;

namespace RegulationApi.Service
{
    public class DistributionService : IDistributionService
    {
        private readonly RegulationDbContext _regulationDbContext;

        public DistributionService(RegulationDbContext regulationDbContext)
        {
            _regulationDbContext = regulationDbContext;
        }

        public Distribution Get(Guid id)
        {
            return _regulationDbContext.Distributions
                .Include(d => d.Store)
                .Include(d => d.DistributionProducts)
                .FirstOrDefault(d => d.Id == id);
        }

        public Distribution Get(Guid id, EntityExpression expression)
        {

            var entity = _regulationDbContext.Distributions
                .Include(d => d.Store)
                .Include(d => d.DistributionProducts)
                .FirstOrDefault(d => d.Id == id);

            ParameterExpression paramExpr = Expression.Parameter(typeof(Distribution), "distribution");
            var d = Expression.Assign(
                Expression.Property(paramExpr, "Sum"),
                Expression.Multiply(Expression.Property(paramExpr, "Sum"), Expression.Constant((decimal)0.9))
            );

            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            settings.Converters.Add(new ExpressionJsonConverter(
                Assembly.GetAssembly(typeof(RegulationService))
            ));

            var e22 = JsonConvert.SerializeObject(expression.SerializedExpression.ToString(), settings);
            var e = JsonConvert.DeserializeObject<Expression>(expression.SerializedExpression.ToString(), settings);

            //Expression.Lambda(e22, new List<ParameterExpression> { paramExpr }).Compile().DynamicInvoke(entity);

            return _regulationDbContext.Distributions
                .Include(d => d.Store)
                .Include(d => d.DistributionProducts)
                .FirstOrDefault(d => d.Id == id);
        }

        public Distribution Post(Guid storeId)
        {
            var distribution = new Distribution();
            
            if (!_regulationDbContext.Stores.Any(s => s.Id == storeId)) return null;
            
            distribution.StoreId = storeId;
            distribution.DistributionProducts = _regulationDbContext.StoreProducts.Include(sp => sp.Product).Where(sp => sp.StoreId == storeId)
                .Select(sp => new DistributionProduct
                {
                    Quantity = sp.Quantity,
                    ProductId = sp.ProductId
                }).ToList();

            distribution = _regulationDbContext.Distributions.Add(distribution).Entity;
            _regulationDbContext.SaveChanges();

            distribution = _regulationDbContext.Distributions
                .Include(d => d.DistributionProducts)
                .ThenInclude(dp => dp.Product).Single(d => d.Id == distribution.Id);

            distribution.Sum = distribution.DistributionProducts.Sum(dp => dp.Product.Price * dp.Quantity);

            distribution = _regulationDbContext.Distributions.Update(distribution).Entity;
            _regulationDbContext.SaveChanges();

            return distribution;
        }

        public Distribution Update(Guid id)
        {
            var distribution = _regulationDbContext.Distributions.Find(id);

            if (!_regulationDbContext.Stores.Any(s => s.Id == distribution.StoreId)) return null;

            distribution.DistributionProducts.Clear();
            distribution.DistributionProducts = _regulationDbContext.StoreProducts
                .Include(sp => sp.Product).Where(sp => sp.StoreId == distribution.StoreId)
                .Select(sp => new DistributionProduct
                {
                    Quantity = sp.Quantity,
                    ProductId = sp.ProductId
                }).ToList();

            distribution = _regulationDbContext.Distributions.Add(distribution).Entity;
            _regulationDbContext.SaveChanges();

            distribution = _regulationDbContext.Distributions
                .Include(d => d.DistributionProducts)
                .ThenInclude(dp => dp.Product).Single(d => d.Id == distribution.Id);

            distribution.Sum = distribution.DistributionProducts.Sum(dp => dp.Product.Price * dp.Quantity);

            distribution = _regulationDbContext.Distributions.Update(distribution).Entity;
            _regulationDbContext.SaveChanges();

            return distribution;
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        private decimal CalculateSum(Distribution distribution)
        {
            return distribution.DistributionProducts.Sum(dp => dp.Quantity * dp.Product.Price);
        }
    }
}