﻿using System;
using System.Collections.Generic;
using System.Linq;
using RegulationApi.DAL;
using RegulationApi.DAL.Model;
using RegulationApi.Service.Interface;

namespace RegulationApi.Service
{
    public class ReturnService : IReturnService
    {
        private readonly RegulationDbContext _regulationDbContext;

        public ReturnService(RegulationDbContext regulationDbContext)
        {
            _regulationDbContext = regulationDbContext;
        }

        public Return Get(Guid id)
        {
            return _regulationDbContext.Returns.FirstOrDefault(d => d.Id == id);
        }

        public Return Post(Guid storeId, Guid distributionId, List<ReturnProduct> products)
        {
            var returnEntity = new Return();

            if (!_regulationDbContext.Stores.Any(s => s.Id == storeId)) return null;
            if (!_regulationDbContext.Distributions.Any(d => d.Id == distributionId)) return null;

            returnEntity.StoreId = storeId;
            returnEntity.DistributionId = distributionId;
            returnEntity.ReturnProducts = products;

            var entity1 = _regulationDbContext.Returns.Add(returnEntity).Entity;
            _regulationDbContext.SaveChanges();

            returnEntity.ReturnProducts = new List<ReturnProduct>();
            returnEntity.ReturnProducts.Add(new ReturnProduct()
            {
                Quantity = 150,
                ProductId = new Guid("369e4b1e-1f96-4457-8aa1-308cf2f91144")
            });


            var entity = _regulationDbContext.Returns.Add(returnEntity).Entity;

            return entity;
        }

        public Return Update(Guid id, List<ReturnProduct> products)
        {
            var returnEntity = _regulationDbContext.Returns.FirstOrDefault(r => r.Id == id);

            if (returnEntity == null) return null;

            returnEntity.ReturnProducts = products;

            var entity = _regulationDbContext.Returns.Update(returnEntity).Entity;
            _regulationDbContext.SaveChanges();

            return entity;
        }

        public bool Delete(Guid id)
        {
            var returnEntity = _regulationDbContext.Returns.FirstOrDefault(r => r.Id == id);

            if (returnEntity == null) return false;

            _regulationDbContext.Returns.Remove(returnEntity);
            _regulationDbContext.SaveChanges();

            return true;
        }
    }
}