﻿using System;
using System.Collections.Generic;
using RegulationApi.DAL.Model;

namespace RegulationApi.Service.Interface
{
    public interface IReturnService
    {
        Return Get(Guid id);
        Return Post(Guid storeId, Guid distributionId, List<ReturnProduct> products);
        Return Update(Guid id, List<ReturnProduct> products);
        bool Delete(Guid id);
    }
}