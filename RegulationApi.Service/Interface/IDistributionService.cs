﻿using System;
using System.Collections.Generic;
using RegulationApi.DAL.Model;

namespace RegulationApi.Service.Interface
{
    public interface IDistributionService
    {
        Distribution Get(Guid id);
        Distribution Get(Guid id, EntityExpression expression);
        Distribution Post(Guid storeId);
        Distribution Update(Guid id);
        bool Delete(Guid id);
    }
}