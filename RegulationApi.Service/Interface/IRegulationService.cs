﻿using System;
using RegulationApi.DAL.Model;

namespace RegulationApi.Service.Interface
{
    public interface IRegulationService
    {
        Store Create(Guid distributionId);
        Store CreateWithInlineExpression(Guid distributionId);
        Store CreateWithInputExpression(Guid distributionId, EntityExpression expression);
    }
}