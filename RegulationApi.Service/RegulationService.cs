﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Aq.ExpressionJsonSerializer;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RegulationApi.DAL;
using RegulationApi.DAL.Model;
using RegulationApi.Service.Interface;

namespace RegulationApi.Service
{
    public class RegulationService : IRegulationService
    {
        private readonly RegulationDbContext _regulationDbContext;

        public RegulationService(RegulationDbContext regulationDbContext)
        {
            _regulationDbContext = regulationDbContext;
        }

        public Store Create(Guid distributionId)
        {
            var distribution = _regulationDbContext.Distributions
                .Include(d => d.Store)
                .ThenInclude(s => s.StoreProducts)
                .Include(d => d.Return)
                .ThenInclude(r => r.ReturnProducts)
                .FirstOrDefault(d => d.Id == distributionId);

            if (distribution == null) return null;

            var store = distribution.Store;

            store.StoreProducts.ForEach(sp =>
                {
                    var returnProduct =
                        distribution.Return.ReturnProducts.FirstOrDefault(rp => rp.ProductId == sp.ProductId);
                    if (returnProduct == null)
                    {
                        sp.Quantity += 2;
                        return;
                    }

                    var decrement =
                        returnProduct.Quantity / sp.Quantity * 100 > 80
                        && sp.Quantity - 2 > 2;

                    var increment =
                        returnProduct.Quantity / sp.Quantity * 100 < 20;
                    sp.Quantity = decrement ? sp.Quantity -= 2 : increment ? sp.Quantity += 2 : sp.Quantity;
                }
            );

            store = _regulationDbContext.Update(store).Entity;
            _regulationDbContext.SaveChanges();

            return store;
        }

        public Store CreateWithInlineExpression(Guid distributionId)
        {
            var distribution = _regulationDbContext.Distributions
                .Include(d => d.Store)
                .ThenInclude(s => s.StoreProducts)
                .Include(d => d.Return)
                .ThenInclude(r => r.ReturnProducts)
                .FirstOrDefault(d => d.Id == distributionId);

            if (distribution == null) return null;

            var store = distribution.Store;

            store.StoreProducts.ForEach(storeProduct =>
                {
                    var returnProduct =
                        distribution.Return.ReturnProducts.FirstOrDefault(rp => rp.ProductId == storeProduct.ProductId);

                    var returnProductParameter = Expression.Parameter(typeof(ReturnProduct), "returnProduct");
                    var storeProductParameter = Expression.Parameter(typeof(StoreProduct), "storeProduct");

                    var expression = Expression.IfThen(
                        Expression.GreaterThan(Expression.Property(returnProductParameter, "Quantity"),
                            Expression.Constant(10)),
                        Expression.Assign(Expression.Property(storeProductParameter, "Quantity"),
                            Expression.Subtract(Expression.Property(storeProductParameter, "Quantity"),
                                Expression.Constant(5)))
                    );

                    Expression.Lambda(expression,
                            new List<ParameterExpression> {returnProductParameter, storeProductParameter}).Compile()
                        .DynamicInvoke(returnProduct, storeProduct);
                }
            );
            store = _regulationDbContext.Update(store).Entity;
            _regulationDbContext.SaveChanges();

            return store;
        }

        public Store CreateWithInputExpression(Guid distributionId, EntityExpression expression)
        {
            var distribution = _regulationDbContext.Distributions
                .Include(d => d.Store)
                .ThenInclude(s => s.StoreProducts)
                .Include(d => d.Return)
                .ThenInclude(r => r.ReturnProducts)
                .FirstOrDefault(d => d.Id == distributionId);

            if (distribution == null) return null;

            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

            settings.Converters.Add(new ExpressionJsonConverter(
                Assembly.GetAssembly(typeof(RegulationService))
            ));

            var store = distribution.Store;

            store.StoreProducts.ForEach(storeProduct =>
            {
                var returnProduct =
                    distribution.Return.ReturnProducts.FirstOrDefault(rp => rp.ProductId == storeProduct.ProductId);

                var returnProductParameter = Expression.Parameter(typeof(ReturnProduct), "returnProduct");
                var storeProductParameter = Expression.Parameter(typeof(StoreProduct), "storeProduct");

                try
                {
                    var deserializedExpression = JsonConvert
                        .DeserializeObject<Expression>(
                            expression.SerializedExpression.ToString(), settings);
                    Expression.Lambda(deserializedExpression,
                            new List<ParameterExpression> {returnProductParameter, storeProductParameter}).Compile()
                        .DynamicInvoke(returnProduct, storeProduct);
                }
                catch (Exception exception)
                {
                    throw new JsonSerializationException(exception.StackTrace);
                }
            });

            store = _regulationDbContext.Update(store).Entity;
            _regulationDbContext.Update(distribution);

            _regulationDbContext.SaveChanges();

            return store;
        }
    }
}